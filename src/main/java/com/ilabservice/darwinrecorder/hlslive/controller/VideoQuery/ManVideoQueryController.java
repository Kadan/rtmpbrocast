//package com.ilabservice.darwinrecorder.hlslive.controller.VideoQuery;
//
//
//import com.ilabservice.darwinrecorder.hlslive.constants.Constants;
//import com.ilabservice.darwinrecorder.hlslive.entity.Video;
//import com.ilabservice.darwinrecorder.hlslive.ffmpeg.FFMPEGUtils;
//import com.ilabservice.darwinrecorder.hlslive.service.VideoService;
//import com.ilabservice.darwinrecorder.hlslive.util.Handler;
//import io.swagger.annotations.ApiOperation;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Service;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import java.io.File;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.*;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
//@RestController
//public class ManVideoQueryController {
//
//    @Value("${videoRecordPath}")
//    private String fileUploadPath;
//
//    @Value("${fileDownloadPath}")
//    private String fileDownloadPath;
//
//    @Autowired
//    VideoService videoService;
//
//    @Autowired
//    FFMPEGUtils ffmpegUtils;
//
//    @Autowired
//    Handler handler;
//    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
//
////    @GetMapping("/find")
////    public void test(){
////        List<Video> videoList = videoService.findByStartTimeAndEndTime(1576683301748L,1576683389756L);
////        videoList.stream().forEach(s->System.out.println(s.getVideo()));
////        return;
////    }
//
//
////    @GetMapping("/manvideos/{cameraId}")
////    @ApiOperation(value="get the event videos by cameraid, starttime (timestamp) and endtime (timestamp)")
//    public ResponseEntity getVideosByCameraID(HttpServletRequest request, @RequestParam(value = "startTime",required = true) long startTime,
//                                              @RequestParam(value="endTime",required = true) long endTime,
//                                              @PathVariable String cameraId){
//
//        List<Video> videos = new ArrayList<Video>();
//
//        if(startTime >= endTime)
//            return new ResponseEntity<String>("query time range is not correct - startTime is ahead of endtime " + startTime + " - endTime " + startTime, HttpStatus.BAD_REQUEST);
//
//        if(StringUtils.isEmpty(cameraId))
//            return new ResponseEntity<String>("Please provide the cameraId " + cameraId, HttpStatus.BAD_REQUEST);
//
//        long vS = 0;
//        long vE = 0;
//        if(String.valueOf(startTime).length()>10 && String.valueOf(endTime).length()>10){
//            String ms = StringUtils.substring(String.valueOf(startTime),10,String.valueOf(startTime).length());
//            String ts = StringUtils.substring(String.valueOf(startTime),0,10);
//            if(Long.valueOf(ms).intValue()>0)
//                vS = Long.valueOf(ts).longValue() + 1;
//            else
//                vS = Long.valueOf(ts).longValue();
//
//
//        }
//        if(String.valueOf(endTime).length()>10){
//            String ms = StringUtils.substring(String.valueOf(endTime),10,String.valueOf(endTime).length());
//            String ts = StringUtils.substring(String.valueOf(endTime),0,10);
//            if(Long.valueOf(ms).intValue()>0)
//                vE = Long.valueOf(ts).longValue() + 1;
//            else
//                vE = Long.valueOf(ts).longValue();
//        }
//
//        long interval = vE - vS;
//        if(interval >7*24*60*60){
//            return new ResponseEntity("The inquiry scope should be within 7 days",HttpStatus.BAD_REQUEST);
//        }
//
//
//        List<Video> videoList = videoService.findByStartTimeAndEndTimeAndCameraId(startTime,endTime,cameraId);
//
//        if(videoList != null ){
//            videoList.stream().forEach(s->{
//                String v = s.getVideo();
//                String st = StringUtils.substringBefore(StringUtils.substringAfterLast(v,"/"),"-");
//                String et = StringUtils.substringBefore(StringUtils.substringAfterLast(v,"-"),".mp4");
//                try {
//                    s.setStartTime(sdf.parse(st).getTime());
//                    s.setEndTime(sdf.parse(et).getTime());
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//                videos.add(s);
//            });
//        }
//
////
////
////
////        if(!fileUploadPath.endsWith(File.separator))
////            fileUploadPath = fileUploadPath+ File.separator;
////        if(!fileDownloadPath.endsWith(File.separator))
////            fileDownloadPath = fileDownloadPath + File.separator;
////
////        String destFolder = fileUploadPath + cameraId+"-manual";
////        String downloadFolder = fileDownloadPath + cameraId+"-manual";
////
////        File destFolders = new File(destFolder);
////        if(destFolders!=null && !destFolders.exists())
////            return new ResponseEntity<String>("The camera ID " + cameraId + " does not exists or the query time range is wrong startTime" +  startTime + " endTime " + endTime, HttpStatus.BAD_REQUEST);
////        List<String> subFolders = Stream.of(destFolders.listFiles()).filter(s -> s.isDirectory()
////                && s.getName().contains("-")
////                && StringUtils.isNumeric(StringUtils.substringBefore(s.getName(),"-"))
////                && StringUtils.isNumeric(StringUtils.substringAfter(s.getName(),"-"))
////                && !StringUtils.contains(s.getName(),"raw")
////                && !StringUtils.contains(s.getName(),"jpg")
////                && !StringUtils.contains(s.getName(),"flv")
////                && !StringUtils.contains(s.getName(),"mp4"))
////                .map(s -> s.getName())
//////                .filter(s->StringUtils.startsWith(s,startTime + "-" + endTime))
////                .filter(s -> (startTime <= Long.valueOf(StringUtils.substringBefore(s, "-")).longValue() &&
////                                endTime > Long.valueOf(StringUtils.substringAfter(s, "-")).longValue() &&
////                                endTime <= Long.valueOf(StringUtils.substringAfter(s, "-")).longValue()
////                        ) ||
////                                (Long.valueOf(StringUtils.substringBefore(s, "-")).longValue() >= startTime &&
////                                        Long.valueOf(StringUtils.substringAfter(s, "-")).longValue() <= endTime
////                                ) ||
////
////                                (startTime >= Long.valueOf(StringUtils.substringBefore(s, "-")).longValue() &&
////                                        startTime < Long.valueOf(StringUtils.substringAfter(s, "-")).longValue() &&
////                                        endTime >= Long.valueOf(StringUtils.substringAfter(s, "-")).longValue()
////                                ) || (startTime >= Long.valueOf(StringUtils.substringBefore(s, "-")).longValue() &&
////                                startTime <= Long.valueOf(StringUtils.substringAfter(s, "-")).longValue() &&
////                                endTime <= Long.valueOf(StringUtils.substringAfter(s, "-")).longValue()
////                        )
////                )
////                .collect(Collectors.toList());
//
//
////        List<String> vs = subFolders.stream().map(s->{
////            File vFile = new File(destFolder + "/" + s + "/" + s + ".flv");
////            if (vFile.exists())
////                return destFolder + "/" + s + "/" + s + ".flv";
////            else{
////                File vFile1 = new File(destFolder + "/" + s + "/" + s + ".mp4");
////                if(vFile1.exists())
////                    return destFolder + "/" + s + "/" + s + ".mp4";
////            }
////            return "";
////
////        }).filter(s->StringUtils.isNotEmpty(s)).collect(Collectors.toList());
////
////        Map<String, Video> videoMap = videoService.findByVideoNameIn(vs);
//
////        subFolders.forEach(f->{
////            if(new File(destFolder + File.separator + f) !=null) {
////                Arrays.stream(new File(destFolder + File.separator + f).listFiles()).filter(s -> StringUtils.startsWith(s.getName(),f) &&
////                        (StringUtils.endsWith(s.getName(),"mp4") || StringUtils.endsWith(s.getName(),"flv"))).forEach(ss -> {
////                    if (ss.exists()) {
////                        Video video = new Video();
////                        video.setVideo(downloadFolder + File.separator + f + File.separator + ss.getName());
////                        video.setEndTime(Long.valueOf(StringUtils.substringAfter(f,"-")));
////                        video.setStartTime(Long.valueOf(StringUtils.substringBefore(f,"-")));
////                        video.setLength((long) handler.getDuration(Long.valueOf(StringUtils.substringBefore(f,"-")), Long.valueOf(StringUtils.substringAfter(f,"-"))));
////                        String image = downloadFolder + File.separator + f + File.separator + StringUtils.substringBefore(ss.getName(), ".") + ".jpg";
////                        video.setImage(image);
////                        videos.add(video);
////                    }
////                });
////            }
////        });
////        subFolders.forEach(s-> {
////            File vFile = new File(destFolder + "/" + s + "/" + s + ".mp4");
////            if (vFile.exists()) {
////                Video video = new Video(downloadFolder + "/" + s + "/" + s + ".mp4",
////                        String.valueOf(StringUtils.substringBefore(s, "-")),
////                        String.valueOf(StringUtils.substringAfter(s, "-")),
////                        downloadFolder + "/" + s + "/" + s + ".jpg",
////                        videoMap.get(destFolder + "/" + s + "/" + s + ".mp4")==null?
////                                ffmpegUtils.getVideoTime(destFolder + "/" + s + "/" + s + ".mp4")
////                                :videoMap.get(destFolder + "/" + s + "/" + s + ".mp4").getLength()
////                );
////                videos.add(video);
////            } else {
////                File vFile1 = new File(destFolder + "/" + s + "/" + s + ".flv");
////                if (vFile1.exists()) {
////                    Video video = new Video(downloadFolder + "/" + s + "/" + s + ".flv",
////                            String.valueOf(StringUtils.substringBefore(s, "-")),
////                            String.valueOf(StringUtils.substringAfter(s, "-")),
////                            downloadFolder + "/" + s + "/" + s + ".jpg",
////                            videoMap.get(destFolder + "/" + s + "/" + s + ".flv")==null?
////                            ffmpegUtils.getVideoTime(destFolder + "/" + s + "/" + s + ".flv")
////                                    :videoMap.get(destFolder + "/" + s + "/" + s + ".flv").getLength()
////                    );
////                    videos.add(video);
////                }
////
////            }
////        });
//        return new ResponseEntity(videos,HttpStatus.OK);
//    }
//
//}
