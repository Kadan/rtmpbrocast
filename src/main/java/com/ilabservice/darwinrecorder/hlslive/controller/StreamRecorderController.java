//package com.ilabservice.darwinrecorder.hlslive.controller;
//
//import com.google.common.io.Files;
//import com.ilabservice.darwinrecorder.hlslive.config.InfluxDbUtils;
//import com.ilabservice.darwinrecorder.hlslive.constants.Constants;
//import com.ilabservice.darwinrecorder.hlslive.cron.DarwinListener;
//import com.ilabservice.darwinrecorder.hlslive.entity.Video;
//import com.ilabservice.darwinrecorder.hlslive.ffmpeg.FFMPEGUtils;
//import com.ilabservice.darwinrecorder.hlslive.measurements.ErrorMeasurement;
//import com.ilabservice.darwinrecorder.hlslive.measurements.RecorderMeasurement;
//import com.ilabservice.darwinrecorder.hlslive.service.VideoService;
//import com.ilabservice.darwinrecorder.hlslive.util.Handler;
//import io.swagger.annotations.ApiOperation;
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.influxdb.dto.Point;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.util.FileCopyUtils;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import sun.nio.ch.IOUtil;
//
//import javax.servlet.http.HttpServletRequest;
//import java.io.*;
//import java.text.DateFormat;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.time.*;
//import java.time.format.DateTimeFormatter;
//import java.util.*;
//import java.util.concurrent.TimeUnit;
//import java.util.stream.Collectors;
//
//@RestController
//public class StreamRecorderController {
//
//    @Autowired
//    Map<String, String> recordingList;
//
//    @Autowired
//    Handler handler;
//
//    @Autowired
//    FFMPEGUtils ffmpegUtils;
//    @Value("${darwinPrefix}")
//    private String darwinPrefix;
//
//    @Value("${videoRecordPath}")
//    private String videoRecordPath;
//
//    @Value("${fileDownloadPath}")
//    private String fileDownloadPath;
//
//    @Value("${videoRecordPath-temp}")
//    private String videoRecordPath_temp;
//
//    @Autowired
//    DarwinListener darwinListener;
//    @Autowired
//    InfluxDbUtils influxDbUtils;
//
//    @Autowired
//    List<String> allStreams;
//
//    @Autowired
//    List<String> allRTSP;
//
//    @Autowired
//    List<String> allRTSPInProgress;
//
//    @Autowired
//    Map<String, Long> requestMap;
//
//    @Autowired
//    Map<String, String> stoppingMap;
//
//    @Autowired
//    VideoService videoService;
//
//    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
//    private static final Logger log = LoggerFactory.getLogger(StreamRecorderController.class);
//
//    @GetMapping("/video/recorder")
//    public List<String> getRecordingList(){
//
//        if(requestMap != null){
//            return requestMap.keySet().stream().collect(Collectors.toList());
//        }
//        else
//            return new ArrayList<String>();
//
//    }
////    @Scheduled(cron = "0/5 * * * * ?")
//    public void daemon(){
//        log.info("prepare all streams ...");
//        allStreams = darwinListener.getAllStream();
//        allStreams.add("rtmp://live.chosun.gscdn.com/live/tvchosun1.stream");
////        passiveList.stream().filter(s->
////            allStreams.contains(s)
////        ).forEach(s->{
////            log.info("the stream exists in passivelist, now checking in" + s);
////            record(s);
////            passiveList.remove(s);
////        });
//    }
//
////    @Scheduled(cron = "0 0 */10 * * ? ")
//    private void removeUnnessaryRawReady(){
//        List<String> fetchList = darwinListener.getAllRTSP();
//        fetchList.stream().forEach(s->{
//            String recordPath = videoRecordPath_temp + File.separator +
//                    org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/") + "-manual" + File.separator;
//            File folder = new File(recordPath);
//            if(requestMap.entrySet().size()==0 || !requestMap.keySet().stream().anyMatch(ss->ss.equalsIgnoreCase(org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/")))) {
//                new Thread(() ->{
//                    //remove unnecessary raw-ready
//                    String currentFile = ffmpegUtils.currentFile(recordPath);
//                    if(folder !=null && folder.isDirectory()){
//                        Arrays.stream(folder.listFiles()).filter(sss->sss.isFile()&&sss.getName().endsWith("mp4")
//                                && !sss.getName().equalsIgnoreCase(currentFile)
//                                && sss.getName().contains("raw-ready")).forEach(sss->{
//                            log.info(s + "No record request ... Removing unnecessary segmentation file " + sss.getName());
//                            sss.delete();
//                        });
//                    }
//
//                }).start();
//            }
//        });
//    }
//
//    @Scheduled(cron = "0/10 * * * * ?")
//    public void ring(){
//        log.info("ring recording ...");
//        List<String> fetchList = darwinListener.getAllRTSP();
////        List<String> fetchList = new ArrayList<>();
////        fetchList.add("rtsp://qz.videostreaming.ilabservice.cloud:554/D72158832");
//
//        fetchList.stream().filter(s-> !stoppingMap.keySet().contains(StringUtils.substringAfterLast(s,"/")) &&
//                 !handler.streamLive(StringUtils.substringAfterLast(s,"/"))).forEach(s->{
//                    //&& !allRTSPInProgress.contains(org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/"))
//
//            log.info("ring recording for " + s);
//            if (!videoRecordPath_temp.endsWith(File.separator))
//                videoRecordPath_temp = videoRecordPath_temp + File.separator;
//            String recordPath = videoRecordPath_temp +
//                    org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/") + "-manual" + File.separator;
//
//            File folder = new File(recordPath);
//
//            if(!folder.exists())
//                folder.mkdirs();
//
//
//            //If the stream is not being recorded,then remove the raw ready segmentation
//            if(requestMap.entrySet().size()==0||!requestMap.keySet().stream().anyMatch(ss->ss.equalsIgnoreCase(org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/")))) {
//                new Thread(() -> {
//                    if (requestMap.entrySet().size() == 0) {
//                        if (folder != null && folder.isDirectory()) {
//                            Arrays.stream(folder.listFiles()).filter(sss -> sss.isFile() && sss.getName().endsWith("mp4")
//                                    && sss.getName().contains("raw-ready")).forEach(sss -> {
//                                log.info("Removing unnecessary segmentation file>>>" + sss.getName());
//                                sss.delete();
//                            });
//                        }
//                    }
//                }).start();
//            }
//
//
//            String file = recordPath;
//            log.info("ring recording for " + recordPath +  "   >>> " +  file);
//            ffmpegUtils.recordRaw(s,file,org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/"));
//
//        });
//    }
//
//    @GetMapping("/video/recordStart/{stream}")
//    public ResponseEntity<String> markRecord(HttpServletRequest request, @PathVariable String stream){
//        if(requestMap.keySet().stream().anyMatch(s->s.contains(stream))){
//            return new ResponseEntity<String>("The stream" + stream + " is already being recorded", HttpStatus.BAD_REQUEST);
//        }
//        String recordPath = videoRecordPath_temp + File.separator +
//                stream + "-manual" + File.separator;
//        String currentFile = ffmpegUtils.currentFile(recordPath);
//
//
//        List<String> fetchList = darwinListener.getAllRTSP();
//        if(!fetchList.stream().anyMatch(s->s.contains(stream))){
//            log.info("The stream is yet to live in darwin will revoke  >>>> " + stream);
//            ErrorMeasurement em = new ErrorMeasurement();
//            em.setTime(System.currentTimeMillis());
//            em.setType("videoRecorder");
//            em.setReason("stream is not live on darwin");
//            em.setName(stream);
//            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//            influxDbUtils.influxDB.write(point);
//            requestMap.put(stream,System.currentTimeMillis());
//            Arrays.stream(new File(recordPath).listFiles()).filter(s->s.isFile() && s.getName().startsWith("raw-ready") && s.getName().endsWith(".mp4") &&
//                    !StringUtils.equalsIgnoreCase(s.getName(),currentFile)).forEach(s->{
//                log.info("removing unnessary raw-ready file " + s.getName());
//                s.delete();
//            });
//            return new ResponseEntity<String>("The stream is yet to live in darwin will revoke " + stream, HttpStatus.BAD_REQUEST);
//        }
//
//
//        Arrays.stream(new File(recordPath).listFiles()).filter(s->s.isFile() && s.getName().startsWith("raw-ready") && s.getName().endsWith(".mp4") &&
//                !StringUtils.equalsIgnoreCase(s.getName(),currentFile)).forEach(s->{
//            log.info("removing unnessary raw-ready file " + s.getName());
//            s.delete();
//        });
//
//        requestMap.put(stream,System.currentTimeMillis());
//
//        RecorderMeasurement rm = new RecorderMeasurement();
//        rm.setTime(System.currentTimeMillis());
//        rm.setEvent("start to record stream");
//        rm.setType("videoRecorder");
//        rm.setName(stream);
//        rm.setRecordingCount(requestMap.keySet().size());
//        rm.setRecordedHours(0);
//        rm.setRecords(0);
//        Point point = Point.measurementByPOJO(rm.getClass()).addFieldsFromPOJO(rm).build();
//        influxDbUtils.influxDB.write(point);
//        return new ResponseEntity<String>("The stream" + stream + " is being recorded", HttpStatus.OK);
//    }
//
//
//    @GetMapping("/video/recordStop/{stream}")
//    public ResponseEntity markStop(HttpServletRequest request, @PathVariable String stream) {
//
//        if (!requestMap.keySet().stream().anyMatch(s -> s.contains(stream))) {
//            return new ResponseEntity<String>("The stream" + stream + " has already been stopped", HttpStatus.OK);
//        }
//
//        if(stoppingMap.keySet().stream().anyMatch(s -> s.contains(stream))){
//            return new ResponseEntity<String>("The stream" + stream + " is in progress of stopping ...", HttpStatus.BAD_REQUEST);
//        }
//
//        if (!videoRecordPath_temp.endsWith(File.separator))
//            videoRecordPath_temp = videoRecordPath_temp + File.separator;
//
//
//        if (!videoRecordPath.endsWith(File.separator))
//            videoRecordPath = videoRecordPath + File.separator;
//
//        String recordPath_temp = videoRecordPath_temp +
//                stream + "-manual" + File.separator;
//
//        String recordPath = videoRecordPath +
//                stream + "-manual" + File.separator;
//
//        String segTime = "3600";
//
//        if(!new File(recordPath).exists())
//            new File(recordPath).mkdirs();
//
//        Long statTime = requestMap.get(stream);
//        Long endTime = System.currentTimeMillis();
//        DecimalFormat df = new DecimalFormat("#.0");
//        try {
//            stoppingMap.put(stream,stream);
//            handler.Stop(stream);
//            //let the file to be written properlly...
//        } catch (Exception e) {
//            e.printStackTrace();
//            ErrorMeasurement em = new ErrorMeasurement();
//            em.setTime(System.currentTimeMillis());
//            em.setType("videoRecorder");
//            em.setReason("unable to stop record the stream");
//            em.setName(stream);
//            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//            influxDbUtils.influxDB.write(point);
//        }
//
//        if (statTime == null) {
//            return new ResponseEntity<String>("The stream" + stream + " has not startted yet, the start time is null ", HttpStatus.BAD_REQUEST);
//        }
//
//        new Thread(() -> {
//            String subFolder = recordPath + statTime + "-" + endTime + File.separator;
//            String downloadPath = fileDownloadPath.endsWith(File.separator)?fileDownloadPath + stream + "-manual" + File.separator+statTime + "-" + endTime + File.separator
//                    :fileDownloadPath+File.separator +stream + "-manual" + File.separator+statTime + "-" + endTime + File.separator;
//
//            String massageFolder = subFolder;
//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        Arrays.stream(new File(recordPath_temp).listFiles()).filter(s -> s != null && s.isFile() && s.getName().endsWith(".mp4")
//                    && s.getName().contains("raw-ready")).forEach(s -> {
//                try {
//                    if (!new File(subFolder).exists())
//                        new File(subFolder).mkdirs();
//                    log.info("copying file " + s.getName() + " to "+massageFolder + File.separator);
//                    //s.renameTo(new File(massageFolder + File.separator + s.getName()));
//                    Files.copy(s,new File(massageFolder + File.separator + s.getName()));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    ErrorMeasurement em = new ErrorMeasurement();
//                    em.setTime(System.currentTimeMillis());
//                    em.setType("videoRecorder");
//                    em.setReason("can not copy file to destination");
//                    em.setName(recordPath_temp + File.separator + s.getName());
//                    Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//                    influxDbUtils.influxDB.write(point);
//                }
//                    });
//            try {
//                Thread.sleep(5000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//
//            if(!new File(massageFolder).exists()){
//                stoppingMap.remove(stream);
//                requestMap.remove(stream);
//                return;
//            }else{
//
//            List<File> recordedFiles = Arrays.stream(new File(massageFolder).listFiles()).filter(s -> s != null && s.isFile() && s.getName().endsWith(".mp4")
//                    && s.getName().contains("raw-ready")).collect(Collectors.toList());
//
//            long firstTouch = statTime;
//            long lastModif = 0;
//            if (recordedFiles != null && recordedFiles.size() > 0) {
//                Collections.sort(recordedFiles);
//
////                int firstRawDuration = ffmpegUtils.getVideoTime(massageFolder + recordedFiles.get(0).getName());
////                if (recordedFiles.size() == 1) {
////                    String outputfile = subFolder + statTime + "-" + endTime + ".mp4";
////                    String image = subFolder + statTime + "-" + endTime + ".jpg";
////
////                    log.info("handling only 1 file ");
////                    try {
////                        int duration = getDuration(statTime, endTime);
////                        if (firstRawDuration <= 0) {//means the first file is not ready ...
////                            try {
////                                Thread.sleep(1000);
////                                firstRawDuration = ffmpegUtils.getVideoTime(massageFolder + recordedFiles.get(0).getName());
////                            } catch (Exception e) {
////                                e.printStackTrace();
////                            }
////                        }
////                        long lastM = new File(massageFolder + recordedFiles.get(0).getName()).lastModified();
////                        int startOffSet = 0;
////
////                        if(endTime > lastM)
////                            startOffSet = firstRawDuration - getDuration(statTime,lastM);
////                        else
////                            startOffSet = firstRawDuration - duration;
////
////                        if (startOffSet <= 0 || firstRawDuration <= 0) startOffSet = 0;
////
////                        int tDuration = duration > firstRawDuration-startOffSet?firstRawDuration-startOffSet:duration;
////                        ffmpegUtils.tailorVideo(massageFolder + recordedFiles.get(0).getName(), outputfile, new Long(startOffSet).intValue(),image,tDuration-1);
////
//////                        Video v = new Video();
//////                        v.setLength(duration);
//////                        v.setStartTime(String.valueOf(statTime));
//////                        v.setEndTime(String.valueOf(endTime));
//////                        v.setImage(subFolder + statTime + "-" + endTime + ".jpg");
//////                        v.setVideo(subFolder + statTime + "-" + endTime + ".mp4");
//////                        videoService.save(v);
////                        RecorderMeasurement rm = new RecorderMeasurement();
////                        rm.setTime(System.currentTimeMillis());
////                        rm.setEvent("successfully stop record the stream");
////                        rm.setType("videoRecorder");
////                        rm.setName(stream);
////                        rm.setRecords(1);
////                        rm.setRecordingCount(requestMap.keySet().size());
////                        rm.setRecordedHours(Double.valueOf(df.format(getDuration(statTime, endTime) / 60)));
////                        Point point = Point.measurementByPOJO(rm.getClass()).addFieldsFromPOJO(rm).build();
////                        influxDbUtils.influxDB.write(point);
////                    } catch (Exception e) {
////                        e.printStackTrace();
////                        ErrorMeasurement em = new ErrorMeasurement();
////                        em.setTime(System.currentTimeMillis());
////                        em.setType("videoRecorder");
////                        em.setReason("unable to merge recorded files");
////                        em.setName(stream);
////                        Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
////                        influxDbUtils.influxDB.write(point);
////                    }
////                }else{
//                        try {
//                                int duration = 0;
//                                String outputfile = subFolder + "readyfile";
//                                try {
//                                    PrintWriter out = new PrintWriter(new BufferedWriter(
//                                            new FileWriter(massageFolder + "index.txt", true)));
//                                    for (int i = 0; i < recordedFiles.size(); i++) {
//                                        String fName = recordedFiles.get(i).getName();
//                                        out.println("file " + fName);
//                                        if(i==recordedFiles.size()-1)
//                                            lastModif = recordedFiles.get(i).lastModified();
//                                    }
//                                    if (out != null)
//                                        out.close();
//
//                                    String outputfile_temp = subFolder + "temp_" + statTime + "-" + endTime + ".mp4";
//                                    if (!new File(massageFolder + "index.txt").exists()) {
//                                        try {
//                                            Thread.sleep(1000);
//                                        } catch (Exception e) {
//                                        }
//                                    }
//                                    ffmpegUtils.concat(massageFolder + "index.txt", outputfile_temp);
//                                    long lastM = new File(massageFolder + recordedFiles.get(recordedFiles.size() - 1).getName()).lastModified();
//                                    int totalDuration = ffmpegUtils.getVideoTime(outputfile_temp);
//                                    int startOffSet = 0;
//                                    if (lastM < endTime)
//                                        startOffSet = totalDuration - getDuration(statTime, lastM);
//                                    else
//                                        startOffSet = totalDuration >= getDuration(statTime, endTime) ? totalDuration - getDuration(statTime, endTime) : 0;
//                                    if (startOffSet < 0) startOffSet = 0;
//                                    duration = totalDuration - startOffSet > getDuration(statTime, endTime) ? getDuration(statTime, endTime) : totalDuration - startOffSet;
//                                    ffmpegUtils.tailorLongVideo(outputfile_temp, outputfile, startOffSet, duration - 1,segTime);
//                                    File images = new File(subFolder);
//                                    List<File> readyFile = Arrays.stream(images.listFiles()).filter(s->s.getName().startsWith("readyfile") && s.getName().endsWith("mp4")).collect(Collectors.toList());
//                                    Collections.sort(readyFile);
//                                    String name = "";
//                                    String raw = "";
//
//
//
//                                    if(readyFile.size()==1){
//                                        Video v = new Video();
//                                        v.setStartTime(statTime);
//                                        v.setEndTime(endTime);
//                                        v.setCameraId(stream);
//                                        name = subFolder+ sdf.format(firstTouch) + "-" + sdf.format(lastModif);
//                                        log.info("renaming file " + readyFile.get(0).getName() + " to " + name + ".mp4");
//                                        raw = subFolder + readyFile.get(0).getName();
//                                        ffmpegUtils.getFirstFrameAsImage(raw,name+".jpg");
//                                        readyFile.get(0).renameTo(new File(name + ".mp4"));
//                                        v.setVideo(downloadPath+sdf.format(firstTouch) + "-" + sdf.format(lastModif)+".mp4");
//                                        v.setImage(downloadPath+sdf.format(firstTouch) + "-" + sdf.format(lastModif)+".jpg");
//                                        v.setLength(((lastModif-firstTouch)/1000));
//                                        videoService.save(v);
//                                    }else for(int i=0;i<readyFile.size();i++){
//                                        Video v = new Video();
//                                        v.setStartTime(statTime);
//                                        v.setEndTime(endTime);
//                                        v.setCameraId(stream);
//                                        if(i==readyFile.size()-1){
//                                            name = subFolder+ sdf.format(firstTouch) + "-" + sdf.format(lastModif);
//                                            log.info("renaming file " + readyFile.get(i).getName() + " to " + name + ".mp4");
//                                            raw = subFolder + readyFile.get(i).getName();
//
//                                            v.setVideo(downloadPath+sdf.format(firstTouch) + "-" + sdf.format(lastModif)+".mp4");
//                                            v.setImage(downloadPath+sdf.format(firstTouch) + "-" + sdf.format(lastModif)+".jpg");
//                                            v.setLength(((lastModif-firstTouch)/1000));
//                                        }
//                                        else{
//                                            name = subFolder+ sdf.format(firstTouch) + "-" + sdf.format(firstTouch+Long.valueOf(segTime)*1000L);
//                                            log.info("renaming file " + readyFile.get(i).getName() + " to " + name + ".mp4");
//                                            raw = subFolder + readyFile.get(i).getName();
//                                            v.setVideo(downloadPath+sdf.format(firstTouch) + "-" + sdf.format(firstTouch+Long.valueOf(segTime)*1000L)+".mp4");
//                                            v.setImage(downloadPath+sdf.format(firstTouch) + "-" + sdf.format(firstTouch+Long.valueOf(segTime)*1000L)+".jpg");
//                                            v.setLength(Long.valueOf(segTime));
//                                        }
//                                        firstTouch = firstTouch + Long.valueOf(segTime)*1000L;
//                                        ffmpegUtils.getFirstFrameAsImage(raw,name + ".jpg");
//                                        readyFile.get(i).renameTo(new File(name + ".mp4"));
//                                        videoService.save(v);
//                                    }
//                            }catch(Exception e){
//                                    e.printStackTrace();
//                                    ErrorMeasurement em = new ErrorMeasurement();
//                                    em.setTime(System.currentTimeMillis());
//                                    em.setType("videoRecorder");
//                                    em.setReason("unable to merge recorded files");
//                                    em.setName(stream);
//                                    Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//                                    influxDbUtils.influxDB.write(point);
//                            }
//                            RecorderMeasurement rm = new RecorderMeasurement();
//                            rm.setTime(System.currentTimeMillis());
//                            rm.setEvent("successfully stop record the stream");
//                            rm.setType("videoRecorder");
//                            rm.setName(stream);
//                            rm.setRecords(1);
//                            rm.setRecordingCount(requestMap.keySet().size());
//                            rm.setRecordedHours(Double.valueOf(df.format(duration / 60)));
//                            Point point = Point.measurementByPOJO(rm.getClass()).addFieldsFromPOJO(rm).build();
//                            influxDbUtils.influxDB.write(point);
//
//                        } catch (Exception e1) {
//                            e1.printStackTrace();
//                            ErrorMeasurement em = new ErrorMeasurement();
//                            em.setTime(System.currentTimeMillis());
//                            em.setType("videoRecorder");
//                            em.setReason("unable to merge recorded files");
//                            em.setName(stream);
//                            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//                            influxDbUtils.influxDB.write(point);
//                        }
////                    }
//
//                Arrays.stream(new File(recordPath_temp).listFiles()).filter(s -> s != null && s.isFile() && s.getName().endsWith(".mp4")
//                        && s.getName().contains("raw-ready")).forEach(s->s.delete());
//                stoppingMap.remove(stream);
//                requestMap.remove(stream);
//                }
//                ErrorMeasurement em = new ErrorMeasurement();
//                em.setTime(System.currentTimeMillis());
//                em.setType("videoRecorder");
//                em.setReason("unable to merge recorded files");
//                em.setName(stream);
//                Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//                influxDbUtils.influxDB.write(point);
//                Arrays.stream(new File(recordPath_temp).listFiles()).filter(s -> s != null && s.isFile() && s.getName().endsWith(".mp4")
//                && s.getName().contains("raw-ready")).forEach(s->s.delete());
//            stoppingMap.remove(stream);
//            requestMap.remove(stream);
//            }
//            }).start();
//        return new ResponseEntity("Recording has been stopped for " + stream , HttpStatus.OK);
//
//    }
//
//
//    public int getDuration(long startTime, long endTime){
//        long diff;
//        if (startTime < endTime) {
//            diff = endTime - startTime;
//        } else {
//            diff = 0;
//        }
//
//        return Long.valueOf(diff%1000==0?diff/1000:diff/1000+1).intValue();
//    }
//
//}
