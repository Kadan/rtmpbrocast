package com.ilabservice.darwinrecorder.hlslive.controller;

import com.ilabservice.darwinrecorder.hlslive.config.InfluxDbUtils;
import com.ilabservice.darwinrecorder.hlslive.constants.Constants;
import com.ilabservice.darwinrecorder.hlslive.cron.DarwinListener;
import com.ilabservice.darwinrecorder.hlslive.ffmpeg.PushUtils;
import com.ilabservice.darwinrecorder.hlslive.measurements.ErrorMeasurement;
import com.ilabservice.darwinrecorder.hlslive.measurements.RtmpMeasurement;
import com.ilabservice.darwinrecorder.hlslive.util.Handler;
import org.influxdb.dto.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
public class RTMPPusher {
    private static final Logger log = LoggerFactory.getLogger(RTMPPusher.class);
    @Autowired
    PushUtils pushUtils;

    @Autowired
    DarwinListener darwinListener;

    @Autowired
    Handler handler;

    @Autowired
    InfluxDbUtils influxDbUtils;


    @Value("${darwinPrefix}")
    private String darwinPrefix;
    @Autowired
    List<String> rtmpInProgressList;

    @Scheduled(cron = "0/6 * * * * ?")
    public void pushRTMP(){
//-f flv -vcodec copy -acodec copy -ac 1 -bsf:a aac_adtstoasc rtmp
        log.info("push RTMP Corn started ...");
        List<String> fetchList = darwinListener.getAllRTSP();
        fetchList.stream().filter(s->!handler.rtmpInProgress(org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/"))
                && rtmpInProgressList.contains(org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/"))).forEach(
                s->{
                    log.info("push  " + s + " to RTMP Server ");
                    rtmpInProgressList.add(org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/"));
                    pushUtils.push(s,org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/"));
                }
        );

    }

    @GetMapping("/rtmpStart/{stream}")
    public ResponseEntity<String> startRTMP(HttpServletRequest request, @PathVariable String stream){
        if(rtmpInProgressList.contains(stream)){
            ErrorMeasurement em = new ErrorMeasurement();
            em.setTime(System.currentTimeMillis());
            em.setType("rtmp");
            em.setReason("stream already on live");
            em.setName(stream);
            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
            influxDbUtils.influxDB.write(point);
            return new ResponseEntity<String>("The stream " + stream + " is already on live ...", HttpStatus.BAD_REQUEST);
        }
        rtmpInProgressList.add(stream);
        List<String> fetchList = darwinListener.getAllRTSP();
        if(fetchList.stream().anyMatch(s->s.contains(stream))){
            pushUtils.push(darwinPrefix+stream,stream);
            RtmpMeasurement rm = new RtmpMeasurement();
            rm.setName(stream);
            rm.setTime(System.currentTimeMillis());
            rm.setTotalBroadcasting(rtmpInProgressList.size());
            rm.setType("rtmp");
            Point point = Point.measurementByPOJO(rm.getClass()).addFieldsFromPOJO(rm).build();
            influxDbUtils.influxDB.write(point);
            return new ResponseEntity<String>("Started to push " + stream, HttpStatus.OK);
        }
        else{
            ErrorMeasurement em = new ErrorMeasurement();
            em.setTime(System.currentTimeMillis());
            em.setType("rtmp");
            em.setReason("stream not alive on darwin");
            em.setName(stream);
            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
            influxDbUtils.influxDB.write(point);

            return new ResponseEntity<String>("The stream " + stream + " dose not exists on darwin ...", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/rtmpStop/{stream}")
    public ResponseEntity<String> stopRTMP(HttpServletRequest request, @PathVariable String stream){
        if(!rtmpInProgressList.contains(stream)){
            ErrorMeasurement em = new ErrorMeasurement();
            em.setTime(System.currentTimeMillis());
            em.setType("rtmp");
            em.setReason("stream already been stopped");
            em.setName(stream);
            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
            influxDbUtils.influxDB.write(point);
            return new ResponseEntity<String>("The stream " + stream + " is already been stopped ...", HttpStatus.BAD_REQUEST);
        }

        List<String> fetchList = darwinListener.getAllRTSP();
        if(fetchList.stream().anyMatch(s->s.contains(stream))){
            rtmpInProgressList.remove(stream);
            handler.stopRTMP(stream);
            rtmpInProgressList.remove(stream);
            return new ResponseEntity<String>("The  " + stream + " rtmp has been stopped", HttpStatus.OK);
        }
        else
            return new ResponseEntity<String>("The stream " + stream + " dose not exists on darwin ...", HttpStatus.BAD_REQUEST);
    }
}
