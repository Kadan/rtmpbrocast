//package com.ilabservice.darwinrecorder.hlslive.controller.VideoQuery;
//
//import com.alibaba.fastjson.JSONObject;
//import com.ilabservice.darwinrecorder.hlslive.config.InfluxDbUtils;
//import com.ilabservice.darwinrecorder.hlslive.constants.Constants;
//import com.ilabservice.darwinrecorder.hlslive.cron.DarwinListener;
//import com.ilabservice.darwinrecorder.hlslive.entity.FailureCounts;
//import com.ilabservice.darwinrecorder.hlslive.entity.RTMP;
//import com.ilabservice.darwinrecorder.hlslive.entity.Stat;
//import com.ilabservice.darwinrecorder.hlslive.entity.TopKFailureStreaming;
//import com.ilabservice.darwinrecorder.hlslive.measurements.*;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.apache.commons.lang3.StringUtils;
//import org.influxdb.dto.Point;
//import org.influxdb.dto.QueryResult;
//import org.influxdb.impl.InfluxDBResultMapper;
//import org.json.JSONArray;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.client.RestTemplate;
//
//import javax.servlet.http.HttpServletRequest;
//import java.math.BigDecimal;
//import java.util.*;
//import java.util.stream.Collectors;
//
////@RestController
////@RequestMapping(value="/video")
////@Api(value="video statistic query")
//public class StatController {
//    @Autowired
//    InfluxDbUtils influxDbUtils;
//
//    @Autowired
//    InfluxDBResultMapper resultMapper;
//
//    @Autowired
//    List<String> darwinList;
//
//    @Autowired
//    DarwinListener darwinListener;
//
//    //recording count
//    @Autowired
//    Map<String, Long> requestMap;
//
//    @Autowired
//    List<String> rtmpInProgressList;
//
//    @Autowired
//    List<String> rtmpStatList;
//
//    @Value("${rtmpServer}")
//    private String rtmpServer;
//
//    @GetMapping("/playing/count")
//    @ApiOperation(value="get the playing counts from rtmp server")
//    public ResponseEntity<String> getPlayingCount(HttpServletRequest request, @RequestParam(value = "sn",required = false) String sn){
//
//        List<RTMP> rtmpList = new ArrayList<RTMP>();
//        Map<String, Integer> playMap = getRTMPStat();
//        if(sn ==null || StringUtils.isEmpty(sn)){
//
//            rtmpList = playMap.entrySet().stream().map(s->{
//                RTMP rtmp = new RTMP();
//                rtmp.setName(s.getKey());
//                if(rtmpInProgressList.contains(s.getKey()))
//                    rtmp.setCount(s.getValue()-1<0?0:s.getValue()-1);
//                else
//                    rtmp.setCount(s.getValue());
//                return rtmp;
//            }).collect(Collectors.toList());
//        }else if(!playMap.keySet().contains(sn)){
//            return new ResponseEntity(sn +" is not broadcasting", HttpStatus.BAD_REQUEST);
//        }else if(playMap.keySet().contains(sn)){
//            rtmpList = playMap.entrySet().stream().filter(s->s.getKey().equalsIgnoreCase(sn)).map(
//                    s->{
//                        RTMP r = new RTMP();
//                        if(rtmpInProgressList.contains(s.getKey()))
//                            r.setCount(s.getValue()-1<0?0:s.getValue()-1);
//                        else
//                            r.setCount(s.getValue());
//                        r.setName(sn);
//                        return r;
//                    }
//            ).collect(Collectors.toList());
//
//        }
//
//        return new ResponseEntity(rtmpList, HttpStatus.OK);
//    }
//
//
//
//    @GetMapping("/stats")
//    @ApiOperation(value="get the video service statistics")
//    public ResponseEntity<String> getStat(HttpServletRequest request, @RequestParam(value = "startTime",required = false) long startTime,
//                                          @RequestParam(value="endTime",required = false) long endTime,
//                                          @RequestParam(value="topK",required = false) int topK){
//
//        String defaultDuration = "now()-7d";
//        int defaultTopK = 3;
//        Stat stat = new Stat();
//        if(topK>0)
//            defaultTopK = topK;
//
//        String pad = "0000000000000000000";
//
//        String timeCondition="";
//        if(String.valueOf(startTime).length()==10 && String.valueOf(endTime).length()==10){
//            timeCondition = " where time >=" + startTime + "s and time <="+endTime + "s ";
//        }else if(String.valueOf(startTime).length()==13 && String.valueOf(endTime).length()==13){
//            timeCondition = " where time >=" + startTime + "ms and time <="+endTime + "ms ";
//        }
//        else
//            timeCondition= " where time > (now()-7d) and time < now() ";
//
//
//        stat.setClusterSize(darwinList.size());
//        stat.setStreamingCount(darwinListener.getAllStream().size());
//        stat.setDecodingCount(rtmpInProgressList.size());
//        stat.setRecordingCount(requestMap.keySet().size());
//
//        //maxBroadcasting
//        Map<String,Integer> rtmpStatMap = getRTMPStat();
//        int max = 0;
//        if(rtmpStatMap!=null){
//            Optional o = rtmpStatMap.entrySet().stream().max(Map.Entry.comparingByValue());
//            if(o!=null && o.isPresent()){
//                Map.Entry<String, Integer> o1 = (Map.Entry<String, Integer>) o.get();
//                if(o!=null) max = o1.getValue();
//            }
//        };
//        stat.setMaxBroadcasting(max);
//
//        //maxStreamingCount
//        String q1 = "select * from "+ Constants.darwin_EVENT_MEASUREMENT + " " + timeCondition;
//        QueryResult qs1 = influxDbUtils.query(q1);
//        List<DarwinMeasurement> darwinMeasurementList = resultMapper.toPOJO(qs1,DarwinMeasurement.class);
//        if(darwinMeasurementList!=null && darwinMeasurementList.size()>0){
//            OptionalInt m = darwinMeasurementList.stream().mapToInt(DarwinMeasurement::getTotalStreams).max();
//            if(m!=null && m.isPresent())
//                stat.setMaxStreamingCount(m.getAsInt());
//        }
//
//        //maxDecodingCount & maxHistoryBroadcasting
//        String q2 = "select * from "+ Constants.rtmp_MEASUREMENT + " " + timeCondition;
//        QueryResult qs2 = influxDbUtils.query(q2);
//        List<RtmpMeasurement> rtmpMeasurementList = resultMapper.toPOJO(qs2,RtmpMeasurement.class);
//        if(rtmpMeasurementList!=null && rtmpMeasurementList.size()>0){
//            OptionalInt m = rtmpMeasurementList.stream().mapToInt(RtmpMeasurement::getTotalBroadcasting).max();
//            if(m!=null && m.isPresent())
//                stat.setMaxBroadcasting(m.getAsInt());
//            OptionalInt m1 = rtmpMeasurementList.stream().mapToInt(RtmpMeasurement::getMaxBroadcasting).max();
//            if(m1!=null && m1.isPresent())
//                stat.setMaxBroadcasting(m1.getAsInt());
//        }
//
//
//        //maxDecodingCount, total recoded files, total hours
//        String q3 = "select * from "+ Constants.recorder_MEASUREMENT + " " + timeCondition;
//        QueryResult qs3 = influxDbUtils.query(q3);
//        List<RecorderMeasurement> recorderMeasurementList = resultMapper.toPOJO(qs3,RecorderMeasurement.class);
//        if(recorderMeasurementList!=null && recorderMeasurementList.size()>0){
//            OptionalInt m = recorderMeasurementList.stream().mapToInt(RecorderMeasurement::getRecordingCount).max();
//            if(m!=null && m.isPresent())
//                stat.setMaxRecordingCount(m.getAsInt());
//            int m1 = recorderMeasurementList.stream().mapToInt(RecorderMeasurement::getRecords).sum();
//            stat.setTotalRecords(m1);
//            double th = recorderMeasurementList.stream().mapToDouble(RecorderMeasurement::getRecordedHours).sum();
//            th = new BigDecimal(th/60).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
//            stat.setTotalRecordingHours(th);
//        }
//
//        //total events
//        String q4 = "select * from "+ Constants.event_MEASUREMENT + " " + timeCondition;
//        QueryResult qs4 = influxDbUtils.query(q4);
//        List<EventMeasurement> eventMeasurementList = resultMapper.toPOJO(qs4,EventMeasurement.class);
//        if(eventMeasurementList!=null && eventMeasurementList.size()>0){
//            int all = eventMeasurementList.stream().mapToInt(EventMeasurement::getEvent).sum();
//            stat.setTotalEvents(all);
//        }
//
//        //errors
//        String q5 = "select * from "+ Constants.error_MEASUREMENT + " " + timeCondition;
//        QueryResult qs5 = influxDbUtils.query(q5);
//        List<ErrorMeasurement> errorMeasurementList = resultMapper.toPOJO(qs5,ErrorMeasurement.class);
//        if(errorMeasurementList!=null && errorMeasurementList.size()>0){
//            Map<String, List<ErrorMeasurement>> resultList = errorMeasurementList.stream().collect(Collectors.groupingBy(ErrorMeasurement::getReason));
//            Map<String,Integer> rel = new HashMap<String,Integer>();
//            resultList.keySet().stream().forEach(s->{
//                rel.put(s,resultList.get(s).size());
//            });
//
//            List<String> sortedByCount =
//                    rel.entrySet().stream().sorted(Map.Entry.<String,Integer>comparingByValue().reversed()).map(e->e.getKey()).collect(Collectors.toList());
//
//            List<String> sortedByCountP = new ArrayList<String>();
//
//            if(sortedByCount!=null && sortedByCount.size()>0){
//
//                if(sortedByCount.size()>topK)
//                    sortedByCountP = sortedByCount.subList(0,topK-1);
//                else
//                    sortedByCountP = sortedByCount;
//
//            }
//
//            List<FailureCounts> failureCountsList = new ArrayList<FailureCounts>();
//            sortedByCountP.stream().forEach(s->{
//                FailureCounts failureCounts = new FailureCounts();
//                failureCounts.setCount(rel.get(s));
//                failureCounts.setReason(s);
//                failureCountsList.add(failureCounts);
//            });
//            stat.setFailureCounts(failureCountsList);
//
//            //topK fail stream
//            Map<String, List<ErrorMeasurement>> resultList1 = errorMeasurementList.stream().collect(Collectors.groupingBy(ErrorMeasurement::getName));
//            Map<String,Integer> rel1 = new HashMap<String,Integer>();
//            resultList1.keySet().stream().forEach(s->{
//                rel1.put(s,resultList1.get(s).size());
//            });
//
//            List<String> sortedByCount1 =
//                    rel1.entrySet().stream().sorted(Map.Entry.<String,Integer>comparingByValue().reversed()).map(e->e.getKey()).collect(Collectors.toList());
//
//            List<String> sortedByCount11= new ArrayList<String>();
//            if(sortedByCount1!=null && sortedByCount1.size()>0){
//                if(sortedByCount1.size()>topK)
//                    sortedByCount11 = sortedByCount1.subList(0,topK-1);
//                else
//                    sortedByCount11 = sortedByCount1;
//            }
//
//            List<TopKFailureStreaming> topKFailureStreamingList = new ArrayList<TopKFailureStreaming>();
//
//            sortedByCount11.stream().forEach(s->{
//                TopKFailureStreaming topKFailureStreaming = new TopKFailureStreaming();
//                topKFailureStreaming.setCount(rel1.get(s));
//                topKFailureStreaming.setName(s);
//                topKFailureStreamingList.add(topKFailureStreaming);
//            });
//
//            stat.setTopKFailureStreaming(topKFailureStreamingList);
//        }
//
//        return new ResponseEntity(stat, HttpStatus.OK);
//    }
//
//
////    @Scheduled(cron = "0/10 * * * * ?")
//    private void rtmpStatCron(){
//        Map<String,Integer> rtmpStatMap = getRTMPStat();
//        int max = 0;
//        if(rtmpStatMap!=null){
//            Optional o = rtmpStatMap.entrySet().stream().max(Map.Entry.comparingByValue());
//            if(o!=null && o.isPresent()){
//                Map.Entry<String, Integer> o1 = (Map.Entry<String, Integer>) o.get();
//                if(o!=null){ max = o1.getValue();
//                RtmpMeasurement rm = new RtmpMeasurement();
//                rm.setName(o1.getKey());
//                rm.setTime(System.currentTimeMillis());
//                rm.setTotalBroadcasting(rtmpInProgressList.size());
//                rm.setMaxBroadcasting(max);
//                rm.setType("rtmp");
//                Point point = Point.measurementByPOJO(rm.getClass()).addFieldsFromPOJO(rm).build();
//                influxDbUtils.influxDB.write(point);
//                }
//            }
//        };
//    }
//
//
////    public static void main(String[] args){
////        new StatController().getRTMPStat1();
////    }
//
//
//
//    private Map<String,Integer> getRTMPStat1(){
//        Map<String,Integer> rtmpStatMap = new HashMap<String,Integer>();
//            List<String> list = new ArrayList<String>();
//            list.add("http://40.73.41.176:8080/stat");
//        list.stream().distinct().forEach(s->{
//                String xml = new RestTemplate().getForObject(s, String.class);
//                String xml1= StringUtils.substringBetween(xml,"<application>\r\n<name>hls</name>\r\n","\r\n</application>");
//            org.json.JSONObject o111 = org.json.XML.toJSONObject(xml1).getJSONObject("live");
//            int totalcount  = StringUtils.countMatches(xml1,"<stream>");
//            int totalcount1  = StringUtils.countMatches(xml1,"</stream>");
//            if(totalcount!=totalcount1)
//                totalcount=totalcount1;
//            if(totalcount>1) {
//
//                try{
//                    org.json.JSONArray jsonArray = org.json.XML.toJSONObject(xml1).getJSONObject("live").getJSONArray("stream");
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        org.json.JSONObject o = jsonArray.getJSONObject(i);
//                        String name = String.valueOf(jsonArray.getJSONObject(i).get("name"));
//                        int cts = jsonArray.getJSONObject(i).getInt("nclients");
//                        rtmpStatMap.put(name, cts);
//                    }
//                }catch(Exception e){
//
//                }
//
//            }else if(totalcount==1){
//                org.json.JSONObject o = org.json.XML.toJSONObject(xml1).getJSONObject("live").getJSONObject("stream");
//                String name = String.valueOf(o.get("name"));
//                int cts = o.getInt("nclients");
//                rtmpStatMap.put(name, cts);
//            }
//        });
//        return rtmpStatMap;
//    }
//
////    public static void main(String[] args){
////
////    }
////
//
//
//    private Map<String,Integer> getRTMPStat(){
//        Map<String,Integer> rtmpStatMap = new HashMap<String,Integer>();
//        if(rtmpStatList ==null || rtmpStatList.size()<=0){
//            return new HashMap<String,Integer>();
//        }else
//        rtmpStatList.stream().distinct().forEach(s->{
//            String xml = new RestTemplate().getForObject(s, String.class);
//            String xml1= StringUtils.substringBetween(xml,"<application>\r\n<name>hls</name>\r\n","\r\n</application>");
////            int totalcount = org.json.XML.toJSONObject(xml1).getJSONObject("live").getInt("nclients");
//            int totalcount  = StringUtils.countMatches(xml1,"<stream>");
//            int totalcount1  = StringUtils.countMatches(xml1,"</stream>");
//            if(totalcount!=totalcount1)
//                totalcount=totalcount1;
//            if(totalcount>1) {
//                org.json.JSONArray jsonArray = org.json.XML.toJSONObject(xml1).getJSONObject("live").getJSONArray("stream");
//                for (int i = 0; i < jsonArray.length(); i++) {
//                    org.json.JSONObject o = jsonArray.getJSONObject(i);
//                    String name = String.valueOf(jsonArray.getJSONObject(i).get("name"));
//                    int cts = jsonArray.getJSONObject(i).getInt("nclients");
//                    rtmpStatMap.put(name, cts);
//                }
//            }else if(totalcount==1){
//                org.json.JSONObject o = org.json.XML.toJSONObject(xml1).getJSONObject("live").getJSONObject("stream");
//                String name = String.valueOf(o.get("name"));
//                int cts = o.getInt("nclients");
//                rtmpStatMap.put(name, cts);
//            }
//        });
//        return rtmpStatMap;
//    }
//
//}
