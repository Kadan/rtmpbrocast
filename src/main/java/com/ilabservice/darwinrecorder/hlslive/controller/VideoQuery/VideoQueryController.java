//package com.ilabservice.darwinrecorder.hlslive.controller.VideoQuery;
//
//import com.ilabservice.darwinrecorder.hlslive.constants.Constants;
//import com.ilabservice.darwinrecorder.hlslive.entity.EvtVideo;
//import com.ilabservice.darwinrecorder.hlslive.entity.Video;
//import com.ilabservice.darwinrecorder.hlslive.ffmpeg.FFMPEGUtils;
//import com.ilabservice.darwinrecorder.hlslive.service.EvtVideoService;
//import com.ilabservice.darwinrecorder.hlslive.service.VideoService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.sound.midi.SysexMessage;
//import java.io.File;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.*;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
////@RestController
////@RequestMapping(value="/query")
////@Api(value="event viode query")
//public class VideoQueryController {
//    @Value("${videoRecordPath}")
//    private String fileUploadPath;
//
//    @Value("${fileDownloadPath}")
//    private String fileDownloadPath;
//
//    @Autowired
//    FFMPEGUtils ffmpegUtils;
//
//    @Autowired
//    EvtVideoService videoService;
//
//    @Value("${ffmpegpath}")
//    private String ffmpegpath;
//
//    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
//
//    @GetMapping("/evtvideoStatus/{cameraId}")
//    @ApiOperation(value=" query the video merge status")
//    public String getVideoTailorStatud(HttpServletRequest request, @RequestParam(value = "startTime",required = true) int startTime,
//                                       @RequestParam(value="endTime",required = true) int endTime,
//                                       @PathVariable String cameraId){
//        if(StringUtils.isEmpty(cameraId))
//            return "Please provide the dcameraId";
//
//        String destFolder = fileUploadPath + cameraId + File.separator + startTime + "-" + endTime;
//
//        File destFolders = new File(destFolder);
//
//        if(destFolders!=null && !destFolders.exists())
//            return "No such directory match the query conditions[cameraid]/[startTime]-[endTime] .. cameraId >>> " + cameraId + " startTime >>> " + startTime + " endTime >>> " + endTime;
//
//        File destFile = new File(destFolder + File.pathSeparator + startTime+"-"+endTime + ".mp4");
//
//        if(destFolders!=null &&!destFolders.exists())
//            return "merged error or merging in progress ... ";
//        if(destFile.exists() && destFile.canRead())
//            return  "READY";
//
//        return "READY";
//    }
//
//
//    @GetMapping("/evtvideos/{cameraId}")
//    //@PostMapping("/evtvideos/{cameraId}")
//    @ApiOperation(value="get the event videos by cameraid, starttime and endtime")
//    public ResponseEntity getVideosByCameraID(HttpServletRequest request, @RequestParam(value = "startTime",required = true) long startTime,
//                                                      @RequestParam(value="endTime",required = true) long endTime,
//                                                      @PathVariable String cameraId){
//
//        List<EvtVideo> videos = new ArrayList<EvtVideo>();
//
//
////        if(String.valueOf(startTime).length()>10){
////            String ms = StringUtils.substring(String.valueOf(startTime),10,String.valueOf(startTime).length());
////            String ts = StringUtils.substring(String.valueOf(startTime),0,10);
////            if(Long.valueOf(ms).intValue()>0)
////                startTime = Long.valueOf(ts).longValue() + 1;
////            else
////                startTime = Long.valueOf(ts).longValue();
////        }
//
////        if(String.valueOf(endTime).length()>10){
////            String ms = StringUtils.substring(String.valueOf(endTime),10,String.valueOf(endTime).length());
////            String ts = StringUtils.substring(String.valueOf(endTime),0,10);
////            if(Long.valueOf(ms).intValue()>0)
////                endTime = Long.valueOf(ts).longValue() + 1;
////            else
////                endTime = Long.valueOf(ts).longValue();
////        }
//        long interval = endTime - startTime;
//        if(interval >7*24*60*60*1000){
//            return new ResponseEntity("The inquiry scope should be within 7 days",HttpStatus.BAD_REQUEST);
//        }
//
//        final long startTime_f = startTime;
//        final long endTime_f = endTime;
//
//
//        if(startTime >= endTime)
//            return new ResponseEntity<String>("query time range is not correct - startTime is ahead of endtime " + startTime + " - endTime " + startTime, HttpStatus.BAD_REQUEST);
//
//        if(StringUtils.isEmpty(cameraId))
//            return new ResponseEntity<String>("Please provide the cameraId " + cameraId, HttpStatus.BAD_REQUEST);
//
//        List<EvtVideo> evtVideoList = videoService.findByStartTimeAndEndTimeAndCameraId(startTime,endTime,cameraId);
//
//        if(evtVideoList != null){
//            evtVideoList.stream().forEach(s->{
//                String v = s.getVideo();
//                String st = StringUtils.substringBefore(StringUtils.substringAfterLast(v,"/"),"-");
//                String et = StringUtils.substringBefore(StringUtils.substringAfterLast(v,"-"),".mp4");
//                s.setStartTime(Long.valueOf(st));
//                s.setEndTime(Long.valueOf(et));
//                videos.add(s);
//            });
//        }
//
//
////        if(!fileUploadPath.endsWith(File.separator))
////            fileUploadPath = fileUploadPath+ File.separator;
////        if(!fileDownloadPath.endsWith(File.separator))
////            fileDownloadPath = fileDownloadPath + File.separator;
////
////        String destFolder = fileUploadPath + cameraId;
////        String downloadFolder = fileDownloadPath + cameraId;
////
////        File destFolders = new File(destFolder);
////        if(destFolders!=null && !destFolders.exists())
////            return new ResponseEntity<String>("The camera ID " + cameraId + " does not exists or the query time range is wrong startTime" +  startTime + " endTime " + endTime, HttpStatus.BAD_REQUEST);
////            //return "No such directory match the query conditions[cameraid]/[startTime]-[endTime] .. cameraId >>> " + cameraId + " startTime >>> " + startTime + " endTime >>> " + endTime;
////
////
////        List<String> subFolders = Stream.of(destFolders.listFiles()).filter(s->s.isDirectory())
////                .map(s->s.getName())
////                .filter(s-> (startTime_f<=Long.valueOf(StringUtils.substringBefore(s,"-")).longValue() &&
////                        endTime_f > Long.valueOf(StringUtils.substringBefore(s,"-")).longValue() &&
////                        endTime_f <=Long.valueOf(StringUtils.substringAfter(s,"-")).longValue()
////                        ) ||
////                        (Long.valueOf(StringUtils.substringBefore(s,"-")).longValue() >= startTime_f &&
////                                Long.valueOf(StringUtils.substringAfter(s,"-")).longValue() <= endTime_f
////                                )||
////
////                        (startTime_f >= Long.valueOf(StringUtils.substringBefore(s,"-")).longValue() &&
////                                startTime_f < Long.valueOf(StringUtils.substringAfter(s,"-")).longValue() &&
////                                endTime_f >= Long.valueOf(StringUtils.substringAfter(s,"-")).longValue()
////                                ) ||(
////                        startTime_f >= Long.valueOf(StringUtils.substringBefore(s,"-")).longValue() &&
////                                startTime_f <= Long.valueOf(StringUtils.substringAfter(s,"-")).longValue() &&
////                                endTime_f <= Long.valueOf(StringUtils.substringAfter(s,"-")).longValue()
////                        )
////
////                )
////                .collect(Collectors.toList());
////
////        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
////
////        List<String> vs = subFolders.stream().map(s->{
////            String name = simpleDateFormat.format(new Date(Long.valueOf(StringUtils.substringBefore(s,"-")).longValue())) + "-" +
////                    simpleDateFormat.format(new Date(Long.valueOf(StringUtils.substringAfter(s,"-")).longValue()));
//////            System.out.println(destFolder+File.separator+s + File.separator+ s + ".mp4");
////            File vFile = new File(destFolder+File.separator+s + File.separator+ name + ".mp4");
////            if(vFile == null ||!vFile.exists())
////                return "";
////            else return destFolder+File.separator+s + File.separator+ name + ".mp4";
////        }).filter(s->StringUtils.isNotEmpty(s)).collect(Collectors.toList());
////
//////        vs.stream().forEach(s->System.out.println(">>>" + s));
////        Map<String,Video> vMap = videoService.findByVideoNameIn(vs);
////        //vMap.keySet().stream().forEach(s-> System.out.println(s));
////        subFolders.forEach(s->{
////            String name = simpleDateFormat.format(new Date(Long.valueOf(StringUtils.substringBefore(s,"-")).longValue())) + "-" +
////            simpleDateFormat.format(new Date(Long.valueOf(StringUtils.substringAfter(s,"-")).longValue()));
////            File vFile = new File(destFolder+File.separator+s + File.separator+ name + ".mp4");
////            if(vFile == null ||!vFile.exists())
////                name = s;
////            long st = Long.valueOf(StringUtils.substringBefore(s,"-"))/1000;
////            long et = Long.valueOf(StringUtils.substringAfter(s,"-"))/1000;
////            int length = (int) (et-st);
////
////            Video video = new Video();
////            video.setImage( downloadFolder+File.separator+ s+ File.separator + Constants.firstFrameImgName);
////            video.setVideo(downloadFolder+File.separator+s + File.separator+ name + ".mp4");
////            video.setStartTime(Long.valueOf(StringUtils.substringBefore(s,"-")));
////            video.setEndTime(Long.valueOf(StringUtils.substringAfter(s,"-")));
////            video.setLength(vMap.get(destFolder+File.separator+s + File.separator+ name + ".mp4")==null?
////                            length
////                            :vMap.get(destFolder+File.separator+s + File.separator+ name + ".mp4").getLength()
////                    );
////            videos.add(video);
////
////        });
//
//        return new ResponseEntity(videos,HttpStatus.OK);
//    }
//
//
////    @GetMapping("/get")
////    public void prepareOnce(){
////        File file = new File(fileUploadPath);
////        Arrays.stream(file.listFiles()).filter(s->s.isDirectory()).collect(Collectors.toList()).stream().forEach(ss->{
////            Arrays.stream(ss.listFiles()).filter(sss->sss.isDirectory()).forEach(ssss->{
////                Arrays.stream(ssss.listFiles()).filter(a->(a.isFile() && !StringUtils.contains(a.getName(),"raw-ready")
////                        &&StringUtils.contains(a.getName(),"-")&& (StringUtils.contains(a.getName(),"mp4") || StringUtils.contains(a.getName(),"flv"))&&a.getName().length()>20))
////                        .forEach(sss->{
////                            System.out.println(sss.getPath());
////                            Video v = new Video();
////                            v.setVideo(sss.getPath());
////                            v.setLength(ffmpegUtils.getVideoTime(sss.getPath()));
////                            v.setStartTime(StringUtils.substringBefore(sss.getName(),"-"));
////                            v.setEndTime(StringUtils.substringBeforeLast(StringUtils.substringAfter(sss.getName(),"-"),"."));
////                            v.setImage(StringUtils.substringBeforeLast(sss.getPath(),".")+".jpg");
////                            videoService.save(v);
////                        });
////                    }
////            );
////        });
////
////    }
//
//}
