//package com.ilabservice.darwinrecorder.hlslive.controller;
//
//
//import com.ilabservice.darwinrecorder.hlslive.config.InfluxDbUtils;
//import com.ilabservice.darwinrecorder.hlslive.constants.Constants;
//import com.ilabservice.darwinrecorder.hlslive.cron.DarwinListener;
//import com.ilabservice.darwinrecorder.hlslive.measurements.DarwinMeasurement;
//import com.ilabservice.darwinrecorder.hlslive.measurements.ErrorMeasurement;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.influxdb.dto.Point;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.client.RestTemplate;
//
//import java.time.Instant;
//import java.util.List;
//import java.util.concurrent.TimeUnit;
//
//@RestController
//@RequestMapping(value="/darwin")
//@Api(value="video statistic query")
//public class DarwinController {
//
//    @Autowired
//    List<String> darwinList;
//
//    @Autowired
//    RestTemplate restTemplate;
//
//    @Autowired
//    InfluxDbUtils influxDbUtils;
//
//    @Autowired
//    DarwinListener darwinListener;
//
//    @GetMapping("/addDarwinNode/{darwinhost}")
//    @ApiOperation(value="get the video service statistics")
//    public ResponseEntity<String> addDarwin(@RequestParam(value = "port",required = true) int port,
//                                          @PathVariable String darwinhost){
//
//        String darwinHost = darwinhost;
//        String darwinPort = String.valueOf(port);
//        String darwinFullPath = "http://" + darwinHost + ":" + darwinPort + Constants.darwinPusherAPISuffix;
//
//        try{
//        String json = restTemplate.getForObject(darwinFullPath, String.class);
//        }catch(Exception e){
//            e.printStackTrace();
//            ErrorMeasurement em = new ErrorMeasurement();
//            em.setTime(System.currentTimeMillis());
//            em.setType("darwin");
//            em.setReason("unable to add darwin host to cluster");
//            em.setName(darwinFullPath);
//            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//            influxDbUtils.influxDB.write(point);
//            return new ResponseEntity("Unable to add darwin host to cluster " + darwinFullPath, HttpStatus.BAD_REQUEST);
//        }
//
//        if(!darwinList.contains(darwinFullPath)){
//            darwinList.add(darwinFullPath);
//            DarwinMeasurement dm = new DarwinMeasurement();
//            dm.setTime(System.currentTimeMillis());
//            dm.setTotalStreams(darwinListener.getAllStream().size());
//            dm.setClusterSize(darwinList.size());
//            dm.setType("darwin");
//            Point point = Point.measurementByPOJO(dm.getClass()).addFieldsFromPOJO(dm).build();
//            influxDbUtils.influxDB.write(point);
//        }
//
//        return new ResponseEntity("darwin host has been added to cluster " + darwinFullPath, HttpStatus.OK);
//    }
//
//    @GetMapping("/removeDarwinNode/{darwinhost}")
//    @ApiOperation(value="get the video service statistics")
//    public ResponseEntity<String> removeDarwin(@RequestParam(value = "port",required = true) int port,
//                                          @PathVariable String darwinhost){
//
//        String darwinHost = darwinhost;
//        String darwinPort = String.valueOf(port);
//        String darwinFullPath = "http://" + darwinHost + ":" + darwinPort + Constants.darwinPusherAPISuffix;
//
//        if(darwinList.contains(darwinFullPath)){
//            darwinList.remove(darwinFullPath);
//            DarwinMeasurement dm = new DarwinMeasurement();
//            dm.setTime(System.currentTimeMillis());
//            dm.setTotalStreams(darwinListener.getAllStream().size());
//            dm.setClusterSize(darwinList.size());
//            dm.setType("darwin");
//            Point point = Point.measurementByPOJO(dm.getClass()).addFieldsFromPOJO(dm).build();
//            influxDbUtils.influxDB.write(point);
//        }
//
//        return new ResponseEntity("darwin host has been removed from cluster " + darwinFullPath, HttpStatus.OK);
//    }
//
//}
