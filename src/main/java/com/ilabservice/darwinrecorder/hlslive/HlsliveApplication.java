package com.ilabservice.darwinrecorder.hlslive;

import com.ilabservice.darwinrecorder.hlslive.cron.DarwinListener;
//import com.ilabservice.darwinrecorder.hlslive.ffmpeg.FFMPEGUtils;
import com.ilabservice.darwinrecorder.hlslive.ffmpeg.PushUtils;
import com.ilabservice.darwinrecorder.hlslive.util.Handler;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFprobe;
import org.influxdb.impl.InfluxDBResultMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//import javax.persistence.Basic;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@SpringBootApplication
@Configuration
@ComponentScan
@EnableScheduling
@EnableSwagger2
public class HlsliveApplication {


	@Value("${ffmpegpath}")
	private String ffmpegpath;

	@Value("${rtmpStats}")
	private String rtmpStats;

	@Value("${ffprobepath}")
	private String ffprobepath;

	@Value("${darwinhost}")
	private String darwinHost;

	@Bean
	public List<String> darwinList(){
		String[] easyDarwinHosts = darwinHost.split(",");
		return Arrays.stream(easyDarwinHosts).distinct().collect(Collectors.toList());
	}

	@Bean
	public Map<String, String> recordingList(){
		return new LinkedHashMap<String,String>();
	}


	@Bean
	public FFmpeg ffmpeg(){

		FFmpeg ffmpeg  = null;
		try {
			ffmpeg = new FFmpeg(ffmpegpath);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ffmpeg;
	}

	@Bean
	FFprobe ffprobe() {
		FFprobe ffprobe = null;

		try {
			ffprobe = new FFprobe(ffprobepath);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ffprobe;
	}


	@Bean
	public RestTemplate restTemplate(){ return new RestTemplate();}


	@Bean
	DarwinListener darwinListener(){return new DarwinListener();}

	@Bean
	InfluxDBResultMapper resultMapper(){
		return new InfluxDBResultMapper();
	}

//	@Bean
//	public FFMPEGUtils ffmpegUtils(){return  new FFMPEGUtils();}

	@Bean
	public PushUtils pushUtils(){return  new PushUtils();}
	@Bean
	List<String> rtmpInProgressList(){ return new ArrayList<>();}

	@Bean
	List<String> allStreams(){ return new ArrayList<>();}

	@Bean
	List<String> rtmpStatList(){
		return Arrays.stream(rtmpStats.split(",")).collect(Collectors.toList()); }

	@Bean
	List<String> allRTSP(){ return new ArrayList<>();}

	@Bean
	List<String> allRTSPInProgress(){ return new ArrayList<>();}

	@Bean
	Map<String, Long> requestMap(){
		return new HashMap<String,Long>();
	}

	Map<String, String> stoppingMap(){
		return new HashMap<String,String>();
	}

	@Bean
	public Handler handler(){return  new Handler();}

	public static void main(String[] args) {
		SpringApplication.run(HlsliveApplication.class, args);

	}




}
