//package com.ilabservice.darwinrecorder.hlslive.service;
//
//import com.ilabservice.darwinrecorder.hlslive.entity.Video;
//
//import java.util.List;
//import java.util.Map;
//
//public interface VideoService {
//
//    List<Video> findByVideoName(String videoName);
//
//    void save(Video video);
//
//    Map<String,Video> findByVideoNameIn(List<String> videoNames);
//
//    List<Video> findByStartTimeAndEndTime(long startTime,long endTime);
//
//    List<Video> findByStartTimeAndEndTimeAndCameraId(long startTime, long endTime,String cameraId);
//
//}
