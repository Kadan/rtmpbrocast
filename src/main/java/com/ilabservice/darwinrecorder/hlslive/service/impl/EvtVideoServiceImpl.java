//package com.ilabservice.darwinrecorder.hlslive.service.impl;
//
//import com.ilabservice.darwinrecorder.hlslive.entity.EvtVideo;
//import com.ilabservice.darwinrecorder.hlslive.entity.Video;
//import com.ilabservice.darwinrecorder.hlslive.repository.EvtVideoRepository;
//import com.ilabservice.darwinrecorder.hlslive.repository.VideoRepository;
//import com.ilabservice.darwinrecorder.hlslive.service.EvtVideoService;
//import com.ilabservice.darwinrecorder.hlslive.service.VideoService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@Service
//public class EvtVideoServiceImpl implements EvtVideoService {
//    @Autowired
//    EvtVideoRepository videoRepository;
//
//    @Override
//    public List<EvtVideo> findByVideoName(String videoName) {
//        return videoRepository.findByVideo(videoName);
//    }
//
//    @Override
//        public void save(EvtVideo video) {
//        videoRepository.save(video);
//        }
//
//    @Override
//    public Map<String,EvtVideo> findByVideoNameIn(List<String> videoNames) {
//        Map<String,EvtVideo> retMap = new HashMap<String,EvtVideo>();
//
//        List<EvtVideo> videos = videoRepository.findByVideoIn(videoNames);
//        if(videos!=null){
//            videos.stream().forEach(s->retMap.put(s.getVideo(),s));
//        }
//        return retMap;
//    }
//
//    @Override
//    public List<EvtVideo> findByStartTimeAndEndTime(long startTime, long endTime) {
//        List<EvtVideo> videoList = videoRepository.findByStartTimeLessThanEqualAndEndTimeGreaterThan(endTime,startTime);
//        return videoList;
//    }
//
//    @Override
//    public List<EvtVideo> findByStartTimeAndEndTimeAndCameraId(long startTime, long endTime,String cameraId) {
//        List<EvtVideo> videoList = videoRepository.findByStartTimeLessThanEqualAndEndTimeGreaterThanAndCameraId(endTime,startTime,cameraId);
//        return videoList;
//    }
//
//}
