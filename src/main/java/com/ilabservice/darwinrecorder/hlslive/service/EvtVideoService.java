//package com.ilabservice.darwinrecorder.hlslive.service;
//
//import com.ilabservice.darwinrecorder.hlslive.entity.EvtVideo;
//import com.ilabservice.darwinrecorder.hlslive.entity.Video;
//
//import java.util.List;
//import java.util.Map;
//
//public interface EvtVideoService {
//
//    List<EvtVideo> findByVideoName(String videoName);
//
//    void save(EvtVideo video);
//
//    Map<String,EvtVideo> findByVideoNameIn(List<String> videoNames);
//
//    List<EvtVideo> findByStartTimeAndEndTime(long startTime, long endTime);
//
//    List<EvtVideo> findByStartTimeAndEndTimeAndCameraId(long startTime, long endTime, String cameraId);
//
//}
