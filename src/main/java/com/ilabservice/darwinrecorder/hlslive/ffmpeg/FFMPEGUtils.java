//package com.ilabservice.darwinrecorder.hlslive.ffmpeg;
//
//import ch.qos.logback.core.util.FileUtil;
//import com.ilabservice.darwinrecorder.hlslive.config.InfluxDbUtils;
//import com.ilabservice.darwinrecorder.hlslive.constants.Constants;
//import com.ilabservice.darwinrecorder.hlslive.controller.StreamRecorderController;
//import com.ilabservice.darwinrecorder.hlslive.measurements.ErrorMeasurement;
//import net.bramp.ffmpeg.FFmpeg;
//import net.bramp.ffmpeg.FFmpegExecutor;
//import net.bramp.ffmpeg.FFprobe;
//import net.bramp.ffmpeg.builder.FFmpegBuilder;
//import net.bramp.ffmpeg.job.FFmpegJob;
//import net.bramp.ffmpeg.probe.FFmpegFormat;
//import net.bramp.ffmpeg.probe.FFmpegProbeResult;
//import net.bramp.ffmpeg.progress.ProgressListener;
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.influxdb.dto.Point;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;
//import org.springframework.util.FileCopyUtils;
//
//import java.io.BufferedReader;
//import java.io.File;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.time.Instant;
//import java.time.LocalDateTime;
//import java.time.ZoneId;
//import java.time.format.DateTimeFormatter;
//import java.util.*;
//import java.util.concurrent.*;
//import java.util.concurrent.atomic.AtomicInteger;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//import java.util.stream.Collectors;
//
//
//@Component
//public class FFMPEGUtils {
//
//    @Autowired
//    FFmpeg ffmpeg;
//
//    @Autowired
//    FFprobe ffprobe;
//
////    @Autowired
////    StreamRecorderController streamRecorderController;
//
//    @Autowired
//    List<String> allRTSPInProgress;
//
//    @Value("${ffmpegpath}")
//    private String ffmpegPath;
//    @Value("${ffprobepath}")
//    private String ffprobePah;
//
//    @Autowired
//    InfluxDbUtils influxDbUtils;
//
//    @Autowired
//    List<String> allRTSP;
//
//    @Autowired
//    Map<String, Long> requestMap;
//    private static DateTimeFormatter localDateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
//
//    @Autowired
//    Map<String,String> recordingList;
//    private static final Logger log = LoggerFactory.getLogger(FFMPEGUtils.class);
//
//    private static ThreadFactory pushRTMPFactory = new ThreadFactory() {
//        private final AtomicInteger counter = new AtomicInteger();
//
//        @Override
//        public Thread newThread(Runnable runable) {
//            return new Thread(runable, "### thread ### " + counter.getAndIncrement());
//        }
//    };
//
//    private static ThreadFactory rawRecordFactory = new ThreadFactory() {
//        private final AtomicInteger counter = new AtomicInteger();
//
//        @Override
//        public Thread newThread(Runnable runable) {
//            return new Thread(runable, "### raw thread ### " + counter.getAndIncrement());
//        }
//    };
//
//    private static ThreadPoolExecutor rawExecutor = new ThreadPoolExecutor(300, 2000, 3, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(100), rawRecordFactory);
//
//
//    public void recordRaw(String inputRTSP, String outputFile,String stream){
//        rawExecutor.submit(
//                new Callable<Integer>() {
//                    @Override
//                    public Integer call() {
//                        //allRTSPInProgress.add(stream);
//                        recordRawVideo(inputRTSP,outputFile,stream);
//                        return 0;
//                    }
//                });
//    }
//
//    public void houseKeeping(String outputDir){
//        File file = new File(outputDir);
//        if(file !=null && file.isDirectory()){
//            List<File> readyFiles = Arrays.stream(file.listFiles()).filter(s->s.isFile()&&s.getName().contains(".mp4")&&s.getName().contains("raw-ready")).collect(Collectors.toList());
//            Collections.sort(readyFiles);
//            if(readyFiles.size()>=2)
//            for(int i=0;i<=readyFiles.size()-2;i++){
//                readyFiles.get(i).delete();
//            }
//        }
//    }
//
//    /**
//     * prepare the threadpool
//     */
//    private static ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(300, 2000, 3, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(100), pushRTMPFactory);
//
//
////    public void record(String inputRTSP, String outputFile,String streamName) {
////
////        log.info("recording " + inputRTSP + "  to " + outputFile);
////        threadPoolExecutor.submit(
////                new Callable<Integer>() {
////                    @Override
////                    public Integer call() {
////                        recordBuilder(inputRTSP,outputFile,streamName);
////                        return 0;
////                    }
////                });
////    }
//
//
////    public static void main(String[] args) throws IOException {
////
//////        System.out.println(new File("/Users/lijunjie/KadanLab/data/raw-ready000.mp4").lastModified());
//////        System.out.println(System.currentTimeMillis());
//////
//////
//////        FFmpegBuilder builder =
//////                new FFmpegBuilder()
//////                        .addInput("/Users/lijunjie/KadanLab/data/kadan-manual/raw-ready002.mp4")
//////                        .setStartOffset(3, TimeUnit.SECONDS)
//////                        .addOutput("/Users/lijunjie/KadanLab/data/kadan-manual/raw-ready11111.mp4")
//////                        .setVideoCodec("copy")
//////                        .setAudioCodec("copy")
//////                        .setAudioChannels(1)
//////                        .setAudioBitStreamFilter("aac_adtstoasc")
//////                        .setFormat("mp4")
//////                        //.setVideoMovFlags("faststart")
//////                        .done();
//////
//////        new FFmpegExecutor(new FFmpeg("/usr/local/bin/ffmpeg"), new FFprobe("/usr/local/bin/ffprobe")).createJob(builder).run();
////        System.out.println(new FFMPEGUtils().getStartNumber("/Users/lijunjie/KadanLab/data"));
////
//////        FFmpegBuilder builder1 =
//////                new FFmpegBuilder()
//////                        .addExtraArgs("-r")
//////                        .addExtraArgs("18")
//////                        .addExtraArgs("-rtsp_transport")
//////                        .addExtraArgs("tcp")
//////                        .setInput("rtsp://40.73.41.176:554/kadan")
//////                        .overrideOutputFiles(true)
//////                        .addOutput("/Users/lijunjie/KadanLab/data/raw-ready%03d.mp4")
//////                        .setVideoCodec("libx264")
////////                        .setAudioCodec("copy")
////////                        .setAudioChannels(1)
////////                        .setAudioBitStreamFilter("aac_adtstoasc")
//////                        .setFormat("segment")
//////                        .addExtraArgs("-segment_time")
//////                        .addExtraArgs("3600")
//////                        .addExtraArgs("-segment_format")
//////                        .addExtraArgs("mp4")
//////                        .addExtraArgs("-segment_wrap")
//////                        .addExtraArgs("24")
//////                        .addExtraArgs("-segment_start_number")
//////                        .addExtraArgs("13")
//////                        .done();
//////        new FFmpegExecutor(new FFmpeg("/usr/local/bin/ffmpeg"), new FFprobe("/usr/local/bin/ffprobe")).createJob(builder1).run();
////    }
////
//
//
//    public void recordRawVideo(String inputRTSP, String outputFile,String stream) {
//
//        String startNumber = "0";
//
//        //Still in progress
////        if(requestMap.keySet().stream().anyMatch(s->s.contains(stream))){
////            startNumber = getStartNumber(outputFile);
////        }
//
//        try {
//            FFmpegBuilder builder1 =
//                    new FFmpegBuilder()
//                            .addExtraArgs("-r")
//                            .addExtraArgs("18")
//                            .addExtraArgs("-rtsp_transport")
//                            .addExtraArgs("tcp")
//                            .setInput(inputRTSP)
//                            .overrideOutputFiles(true)
//                            .addOutput(outputFile + "raw-ready%s%S0.mp4")
////                            .setDuration(12, TimeUnit.HOURS)
//                            .setVideoCodec("copy")
//                            .setAudioCodec("copy")
//                            .setVideoMovFlags("faststart+frag_keyframe")
////                            .setAudioChannels(1)
////                            .setAudioBitStreamFilter("aac_adtstoasc")
//                            .setFormat("segment")
//                            .addExtraArgs("-strftime")
//                            .addExtraArgs("1")
//                            .addExtraArgs("-segment_time")
//                            .addExtraArgs("3600")
//                            .addExtraArgs("-segment_format")
//                            .addExtraArgs("mp4")
////                            .addExtraArgs("-segment_wrap")
////                            .addExtraArgs("8761")
////                            .addExtraArgs("-segment_start_number")
////                            .addExtraArgs(startNumber)
//                            .addExtraArgs("-reset_timestamps")
//                            .addExtraArgs("1")
//                            .addExtraArgs("-map")
//                            .addExtraArgs("0")
//                            .done();
//            new FFmpegExecutor(ffmpeg, ffprobe).createJob(builder1).run();
//
////            if(new File(outputFile).exists() && new File(outputFile).isFile()&& FileUtils.sizeOf(new File(outputFile))>0) {
////                try{
////                    new File(outputFile).renameTo(new File(StringUtils.substringBeforeLast(outputFile, ".flv") + "-ready"
////                            + System.currentTimeMillis() + ".flv"));
////                }catch(Exception fe){fe.printStackTrace();}
////
////                }
////            if(requestMap.keySet().stream().anyMatch(s->s.contains(stream))){
////                //allRTSPInProgress.remove(stream);
////                streamRecorderController.ring();
////            }
//        } catch (Exception e) {
//
//            //e.printStackTrace();
//            log.info("exception during recording raw or stopping the record >>>" + e.getMessage());
//
//            if (requestMap.keySet().stream().anyMatch(s -> s.contains(stream))) {
////                if (new File(outputFile).exists() && new File(outputFile).isFile() && FileUtils.sizeOf(new File(outputFile))>0){
////                    try{
////                        new File(outputFile).renameTo(new File(StringUtils.substringBeforeLast(outputFile, ".flv") + "-ready"
////                            + System.currentTimeMillis() + ".flv"));
////                    }catch(Exception fe){fe.printStackTrace();}
////                }
//                ErrorMeasurement em = new ErrorMeasurement();
//                em.setTime(System.currentTimeMillis());
//                em.setType("videoRecorder");
//                em.setReason("exception while stopping the stream recording");
//                em.setName(stream);
//                Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//                influxDbUtils.influxDB.write(point);
////                streamRecorderController.ring();
//            }
//
//        }
//
//    }
//
//
//    public String currentFile(String outputFile){
//        File destFolder = new File(outputFile);
//        Map<String, Long> filesMap = new HashMap<String, Long>();
//        Arrays.stream(destFolder.listFiles()).filter(s -> s.isFile() && s.getName().contains("raw-ready") && s.getName().endsWith("mp4")).forEach(s -> {
//            filesMap.put(s.getName(), Long.valueOf(StringUtils.substringBetween(s.getName(),"raw-ready",".mp4")));
//        });
//        String fileName = "";
//        try {
//            fileName = filesMap.entrySet().stream().max(Map.Entry.comparingByValue()).get().getKey();
//        } catch (Exception e
//        ) {
//            fileName = "";
//        }
//        return fileName;
//    }
//
//
//
//    public String getStartNumber(String outputFile){
//        File destFolder = new File(outputFile);
//
//        Integer max = 0;
//        try {
//            max = Arrays.stream(destFolder.listFiles()).filter(s -> s.isFile() && s.getName().contains("raw-ready") && s.getName().endsWith("mp4"))
//                    .map(s -> {
//                        return StringUtils.substringBetween(s.getName(), "raw-ready", ".mp4");
//                    }).filter(s -> StringUtils.isNumeric(s)).map(s -> Integer.valueOf(s)).max((a, b) -> {
//                        if (a > b) {
//                            return 1;
//                        } else return -1;
//                    }).get();
//        }catch(Exception e){}
//
//
//        if(destFolder!=null && Arrays.stream(destFolder.listFiles()).anyMatch(s->s.getName().equalsIgnoreCase("raw-ready00000.mp4")) && max==0)
//            max = max+1;
//        else if(destFolder!=null && Arrays.stream(destFolder.listFiles()).anyMatch(s->s.getName().equalsIgnoreCase("raw-ready00000.mp4")) && max>0)
//            max=max+1;
//        else
//            max=0;
//        return String.valueOf(max);
//    }
//
//
//
////    public void start(String stream,String output,String streamName){
////        List<String> commands = new java.util.ArrayList<String>();
////        commands.add(ffmpegPath);
////        commands.add("-n");
////        commands.add("-rtsp_transport");
////        commands.add("tcp");
////        commands.add("-v");
////        commands.add("error");
////        commands.add("-rtbufsize");
////        commands.add("300M");
////        commands.add("-i");
////        //commands.add("rtmp://live.chosun.gscdn.com/live/tvchosun1.stream");
////        commands.add(stream);
////        commands.add("-f");
////        commands.add("mp4");
////        commands.add("-vcodec");
////        commands.add("copy");
////        commands.add("-acodec");
////        commands.add("copy");
////        commands.add("-bsf:a");
////        commands.add("aac_adtstoasc");
////        commands.add("-ac");
////        commands.add("1");
////        commands.add(output);
////        //commands.add("/Users/lijunjie/KadanLAB/data/recorde.mp4");
////        ProcessBuilder builder = new ProcessBuilder();
////        builder.command(commands);
////        try {
////            final Process p = builder.start();
////            recordingList.put(streamName,streamName);
////            log.info("recorder starting for ...."  +stream + " ");
////        } catch (Exception e) {
////            log.info("error while recording stream"  +stream + " bringing up....");
////            e.printStackTrace();
////            if(recordingList.containsKey(streamName)){
////                new StreamRecorderController().record(streamName);
////                //recordBuilder(inputRTSP, outputFile,streamName);
////            }
////        }
////    }
//
//
//
//    public void concat(String input, String output){
//        FFmpegBuilder builder =
//                new FFmpegBuilder()
//                        .setFormat("concat")
//                        .addInput(input)
//                        .addOutput(output)
//                        .setVideoCodec("copy")
//                        .setAudioCodec("copy")
//                        .setFormat("mp4")
//                        .done();
//        FFmpegExecutor executor = null;
//        try {
//            executor = new FFmpegExecutor(ffmpeg,ffprobe);
//        } catch (Exception e) {
//            e.printStackTrace();
//            ErrorMeasurement em = new ErrorMeasurement();
//            em.setTime(System.currentTimeMillis());
//            em.setType("videoRecorder");
//            em.setReason("failed to concat videos");
//            em.setName("videoRecorder");
//            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//            influxDbUtils.influxDB.write(point);
//        }
//        executor.createJob(builder).run();
//    }
//
//
////    public void recordBuilder(String inputRTSP, String outputFile,String streamName) {
////        recordingList.put(streamName,streamName);
////        try{
////            FFmpegBuilder builder1 =
////                    new FFmpegBuilder()
////                            .setInput(inputRTSP)
////                            .overrideOutputFiles(false)
////                            .addOutput(outputFile)
////                            .setDuration(30, TimeUnit.MINUTES)
////                            .setVideoCodec("copy")
////                            .setAudioCodec("copy")
////                            .setAudioChannels(1)
////                            .setAudioBitStreamFilter("aac_adtstoasc")
////                            .setFormat("mp4")
////                            .done();
////            new FFmpegExecutor(ffmpeg,ffprobe).createJob(builder1).run();
////            if(recordingList.containsKey(streamName)){
////                recordingList.remove(streamName);
////                log.info("yet to receive a stop signal, keep recording the stream " + streamName);
////                streamRecorderController.record(streamName);
////                //recordBuilder(inputRTSP, outputFile,streamName);
////            }
////        }catch(Exception e){
////            e.printStackTrace();
////            log.info("error while recording stream"  + streamName + " ");
////            recordingList.remove(streamName);
//////            if(recordingList.containsKey(streamName));{
//////                recordingList.remove(streamName);
//////                streamRecorderController.record(streamName);
//////
//////            }
//////                //recordBuilder(inputRTSP, outputFile,streamName);
////        }
////
////    }
//
//    public void tailorVideo(String input, String output,int startOffSet,String image,int duration) {
//        log.info("tailoring file with offset " + startOffSet );
//
//        FFmpegBuilder builder =
//                new FFmpegBuilder()
//                        .addExtraArgs("-r")
//                        .addExtraArgs("18")
//                        .setInput(input)
//                        .addOutput(output)
//                        .setStartOffset(startOffSet, TimeUnit.SECONDS)
//                        .setDuration(duration,TimeUnit.SECONDS)
//                        .setVideoCodec("copy")
//                        .setAudioCodec("copy")
//                        .setVideoMovFlags("faststart")
////                        .setAudioChannels(1)
////                        .setAudioBitStreamFilter("aac_adtstoasc")
//                        .setFormat("mp4")
//                        //.setVideoMovFlags("faststart")
//                        .addExtraArgs("-reset_timestamps")
//                        .addExtraArgs("1")
//                        .addExtraArgs("-map")
//                        .addExtraArgs("0")
//                        .done();
//        FFmpegExecutor executor = null;
//        try {
//            executor = new FFmpegExecutor(ffmpeg, ffprobe);
//        } catch (Exception e) {
//            e.printStackTrace();
//            ErrorMeasurement em = new ErrorMeasurement();
//            em.setTime(System.currentTimeMillis());
//            em.setType("videoRecorder");
//            em.setReason("failed to tailor video");
//            em.setName("videoRecorder");
//            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//            influxDbUtils.influxDB.write(point);
//
//            log.info("file tailor failed");
//        }
//        executor.createJob(builder).run();
//        getFirstFrameAsImage(output,image);
//
//    }
//
//
//    public void tailorLongVideo(String input, String output,int startOffSet,int duration,String segTime) {
//        log.info("tailoring large file with offset " + startOffSet );
//
//        FFmpegBuilder builder =
//                new FFmpegBuilder()
//                        .addExtraArgs("-r")
//                        .addExtraArgs("18")
//                        .setInput(input)
//                        .addOutput(output + "%05d.mp4")
//                        .setStartOffset(startOffSet, TimeUnit.SECONDS)
//                        .setDuration(duration,TimeUnit.SECONDS)
//                        .setVideoCodec("copy")
//                        .setAudioCodec("copy")
//                        .setVideoMovFlags("faststart+frag_keyframe")
//                        .setFormat("segment")
//                        .addExtraArgs("-segment_time")
//                        .addExtraArgs(segTime)
//                        .addExtraArgs("-segment_format")
//                        .addExtraArgs("mp4")
//                        .addExtraArgs("-segment_wrap")
//                        .addExtraArgs("8761")
//                        .addExtraArgs("-segment_start_number")
//                        .addExtraArgs("0")
//                        .addExtraArgs("-reset_timestamps")
//                        .addExtraArgs("1")
//                        .addExtraArgs("-map")
//                        .addExtraArgs("0")
//                        .done();
//        FFmpegExecutor executor = null;
//        try {
//            executor = new FFmpegExecutor(ffmpeg, ffprobe);
//        } catch (Exception e) {
//            e.printStackTrace();
//            ErrorMeasurement em = new ErrorMeasurement();
//            em.setTime(System.currentTimeMillis());
//            em.setType("videoRecorder");
//            em.setReason("failed to tailor video");
//            em.setName("videoRecorder");
//            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//            influxDbUtils.influxDB.write(point);
//
//            log.info("file tailor failed");
//        }
//        executor.createJob(builder).run();
////        getFirstFrameAsImage(output,image);
//
//    }
//
//    public void getFirstFrameAsImage(String inputVideo, String image){
//        List<String> commands = new java.util.ArrayList<String>();
//        commands.add(ffmpegPath);
//        commands.add("-i");
//        commands.add(inputVideo);
//        commands.add("-ss");
//        commands.add("1");
//        commands.add("-f");
//        commands.add("image2");
////        commands.add("-vframes");
////        commands.add("1");
//        commands.add(image);
//        ProcessBuilder builder = new ProcessBuilder();
//        builder.command(commands);
//        try {
//            final Process p = builder.start();
//            Thread.sleep(500);
//            if(new File(image).exists()){
//                if(p.isAlive())
//                    p.destroy();
//            }else{
//                Thread.sleep(2000);
//                if(new File(image).exists()){
//                    if(p.isAlive())
//                        p.destroy();
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            ErrorMeasurement em = new ErrorMeasurement();
//            em.setTime(System.currentTimeMillis());
//            em.setType("videoRecorder");
//            em.setReason("failed to get image from video");
//            em.setName("videoRecorder");
//            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//            influxDbUtils.influxDB.write(point);
//        }
//    }
//
//
//    public  int getVideoTime(String video_path) {
//
//        try {
//            FFmpegProbeResult probeResult = ffprobe.probe(video_path);
//            FFmpegFormat format = probeResult.getFormat();
//            double d = format.duration;
//            int duration = Double.valueOf(d).intValue();
//            String dd = String.valueOf(d);
//            if(Integer.valueOf(StringUtils.substringAfter(dd,"."))>0)
//                return (duration+1);
//            else
//                return duration;
//
//        } catch (IOException e) {
//            e.printStackTrace();
//            ErrorMeasurement em = new ErrorMeasurement();
//            em.setTime(System.currentTimeMillis());
//            em.setType("videoRecorder");
//            em.setReason("failed to get video length");
//            em.setName("videoRecorder");
//            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//            influxDbUtils.influxDB.write(point);
//            return 0;
//        }
//
////
////        List<String> commands = new java.util.ArrayList<String>();
////        commands.add(ffmpegPath);
////        commands.add("-i");
////        commands.add(video_path);
////        try {
////            ProcessBuilder builder = new ProcessBuilder();
////            builder.command(commands);
////            final Process p = builder.start();
////
////            //从输入流中读取视频信息
////            BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream()));
////            StringBuffer sb = new StringBuffer();
////            String line = "";
////            while ((line = br.readLine()) != null) {
////                sb.append(line);
////            }
////            br.close();
////
////            //从视频信息中解析时长
////            String regexDuration = "Duration: (.*?), start: (.*?), bitrate: (\\d*) kb\\/s";
////            Pattern pattern = Pattern.compile(regexDuration);
////            Matcher m = pattern.matcher(sb.toString());
////            if (m.find()) {
////                int time = getTimelen(m.group(1));
////                log.info(video_path+ ", length："+time+", start@ ："+m.group(2)+", Bits："+m.group(3)+"kb/s");
////                return time;
////            }
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////
////        return 0;
//    }
//
//    private int getTimelen(String timelen) {
//        int min = 0;
//        String strs[] = timelen.split(":");
//        if (strs[0].compareTo("0") > 0) {
//            min += Integer.valueOf(strs[0]) * 60 * 60;//秒
//        }
//        if (strs[1].compareTo("0") > 0) {
//            min += Integer.valueOf(strs[1]) * 60;
//        }
//        if (strs[2].compareTo("0") > 0) {
//            min += Math.round(Float.valueOf(strs[2]));
//        }
//        return min;
//    }
//
//
//}
