package com.ilabservice.darwinrecorder.hlslive.ffmpeg;

import com.ilabservice.darwinrecorder.hlslive.config.InfluxDbUtils;
import com.ilabservice.darwinrecorder.hlslive.measurements.ErrorMeasurement;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import org.influxdb.dto.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;


public class PushUtils {

    @Autowired
    FFmpeg ffmpeg;
    @Autowired
    List<String> rtmpInProgressList;

    @Autowired
    FFprobe ffprobe;

    @Value("${ffmpegpath}")
    private String ffmpegPath;
    @Value("${ffprobepath}")
    private String ffprobePah;

    @Value("${rtmpServer}")
    private String rtmpServer;

    @Autowired
    InfluxDbUtils influxDbUtils;

    private static final Logger log = LoggerFactory.getLogger(PushUtils.class);

    private static ThreadFactory pushRTMPFactory = new ThreadFactory() {
        private final AtomicInteger counter = new AtomicInteger();

        @Override
        public Thread newThread(Runnable runable) {
            return new Thread(runable, "### thread ### " + counter.getAndIncrement());
        }
    };

    private static ThreadPoolExecutor rawExecutor = new ThreadPoolExecutor(300, 2000, 3, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(100), pushRTMPFactory);


    public void push(String inputRTSP, String stream){
        rawExecutor.submit(
                new Callable<Integer>() {
                    @Override
                    public Integer call() {
                        //allRTSPInProgress.add(stream);
                        pushRTMP(inputRTSP,stream);
                        return 0;
                    }
                });
    }

    private int pushRTMP(String inputRTSP, String stream){
        {

            try {
                //RunProcessFunction func = new RunProcessFunction();
                //func.setWorkingDirectory("/Users/lijunjie/easyDarwin/ffmpeg/bin");
                FFmpeg ffmpeg = new FFmpeg(ffmpegPath);
                FFprobe ffprobe = new FFprobe(ffprobePah);
                FFmpegBuilder builder =
                        new FFmpegBuilder()
                                .addExtraArgs("-rtsp_transport")
                                .addExtraArgs("tcp")
                                .setInput(inputRTSP)
                                .addOutput(rtmpServer + stream)
                                .setVideoCodec("copy")
                                .setAudioCodec("copy")
                                .setAudioChannels(1)
                                .setAudioBitStreamFilter("aac_adtstoasc")
                                .setFormat("flv")
                                .done();
                FFmpegExecutor executor = new FFmpegExecutor(ffmpeg, ffprobe);
                executor.createJob(builder).run();
                //rtmpInProgressList.remove(inputRTSP);
                return 0;
            }catch(Exception e){

                if(rtmpInProgressList.contains(stream)){
                    ErrorMeasurement em = new ErrorMeasurement();
                    em.setTime(System.currentTimeMillis());
                    em.setType("rtmp");
                    em.setReason("error while broadcasting stream");
                    em.setName(stream);
                    Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
                    influxDbUtils.influxDB.write(point);
                }
                //e.printStackTrace();
                log.info("error during pushing rtmp or stopping rtmp ...>>>"  + e.getMessage());
                //rtmpInProgressList.remove(inputRTSP);
                return 1;
            }
        }
    }

}
