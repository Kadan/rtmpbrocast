//package com.ilabservice.darwinrecorder.hlslive.ffmpeg;
//
//import com.ilabservice.darwinrecorder.hlslive.config.InfluxDbUtils;
////import com.ilabservice.darwinrecorder.hlslive.controller.StreamRecorderController;
//import com.ilabservice.darwinrecorder.hlslive.measurements.ErrorMeasurement;
//import net.bramp.ffmpeg.FFmpeg;
//import net.bramp.ffmpeg.FFmpegExecutor;
//import net.bramp.ffmpeg.FFprobe;
//import net.bramp.ffmpeg.builder.FFmpegBuilder;
//import net.bramp.ffmpeg.probe.FFmpegFormat;
//import net.bramp.ffmpeg.probe.FFmpegProbeResult;
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.influxdb.dto.Point;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;
//
//import java.io.File;
//import java.io.IOException;
//import java.time.format.DateTimeFormatter;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.*;
//import java.util.concurrent.atomic.AtomicInteger;
//
//
//@Component
//public class FFMPEGUtilsbk {
//
//    @Autowired
//    FFmpeg ffmpeg;
//
//    @Autowired
//    FFprobe ffprobe;
//
//    @Autowired
//    StreamRecorderController streamRecorderController;
//
//    @Autowired
//    List<String> allRTSPInProgress;
//
//    @Value("${ffmpegpath}")
//    private String ffmpegPath;
//    @Value("${ffprobepath}")
//    private String ffprobePah;
//
//    @Autowired
//    InfluxDbUtils influxDbUtils;
//
//    @Autowired
//    List<String> allRTSP;
//
//    @Autowired
//    Map<String, Long> requestMap;
//    private static DateTimeFormatter localDateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
//
//    @Autowired
//    Map<String,String> recordingList;
//    private static final Logger log = LoggerFactory.getLogger(FFMPEGUtilsbk.class);
//
//    private static ThreadFactory pushRTMPFactory = new ThreadFactory() {
//        private final AtomicInteger counter = new AtomicInteger();
//
//        @Override
//        public Thread newThread(Runnable runable) {
//            return new Thread(runable, "### thread ### " + counter.getAndIncrement());
//        }
//    };
//
//    private static ThreadFactory rawRecordFactory = new ThreadFactory() {
//        private final AtomicInteger counter = new AtomicInteger();
//
//        @Override
//        public Thread newThread(Runnable runable) {
//            return new Thread(runable, "### raw thread ### " + counter.getAndIncrement());
//        }
//    };
//
//    private static ThreadPoolExecutor rawExecutor = new ThreadPoolExecutor(300, 2000, 3, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(100), rawRecordFactory);
//
//
//    public void recordRaw(String inputRTSP, String outputFile,String stream){
//        rawExecutor.submit(
//                new Callable<Integer>() {
//                    @Override
//                    public Integer call() {
//                        //allRTSPInProgress.add(stream);
//                        recordRawVideo(inputRTSP,outputFile,stream);
//                        return 0;
//                    }
//                });
//    }
//
//    /**
//     * prepare the threadpool
//     */
//    private static ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(300, 2000, 3, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(100), pushRTMPFactory);
//
//
////    public void record(String inputRTSP, String outputFile,String streamName) {
////
////        log.info("recording " + inputRTSP + "  to " + outputFile);
////        threadPoolExecutor.submit(
////                new Callable<Integer>() {
////                    @Override
////                    public Integer call() {
////                        recordBuilder(inputRTSP,outputFile,streamName);
////                        return 0;
////                    }
////                });
////    }
//
//
////    public static void main(String[] args) throws IOException {
////        FFmpegBuilder builder1 =
////                new FFmpegBuilder()
////                        .addExtraArgs("-rtsp_transport")
////                        .addExtraArgs("tcp")
////                        .setInput("rtsp://40.73.41.176:554/kadan")
////                        .overrideOutputFiles(true)
////                        .addOutput("/Users/lijunjie/KadanLab/data/raw%03d.mp4")
////                        .setVideoCodec("copy")
////                        .setAudioCodec("copy")
////                        .setAudioChannels(1)
////                        .setAudioBitStreamFilter("aac_adtstoasc")
////                        .setFormat("segment")
////                        .addExtraArgs("-segment_time")
////                        .addExtraArgs("60")
////                        .addExtraArgs("-segment_format")
////                        .addExtraArgs("mp4")
////                        .done();
////        new FFmpegExecutor(new FFmpeg("/usr/local/bin/ffmpeg"), new FFprobe("/usr/local/bin/ffprobe")).createJob(builder1).run();
////    }
//
//
//
//    public void recordRawVideo(String inputRTSP, String outputFile,String stream) {
//        try {
//            FFmpegBuilder builder1 =
//                    new FFmpegBuilder()
//                            .addExtraArgs("-rtsp_transport")
//                            .addExtraArgs("tcp")
//                            .setInput(inputRTSP)
//                            .overrideOutputFiles(true)
//                            .addOutput(outputFile)
//                            .setDuration(12, TimeUnit.HOURS)
//                            .setVideoCodec("copy")
//                            .setAudioCodec("copy")
//                            .setAudioChannels(1)
//                            .setAudioBitStreamFilter("aac_adtstoasc")
//                            .setFormat("flv")
//                            .done();
//            new FFmpegExecutor(ffmpeg, ffprobe).createJob(builder1).run();
//
//            if(new File(outputFile).exists() && new File(outputFile).isFile()&& FileUtils.sizeOf(new File(outputFile))>0) {
//                try{
//                    new File(outputFile).renameTo(new File(StringUtils.substringBeforeLast(outputFile, ".flv") + "-ready"
//                            + System.currentTimeMillis() + ".flv"));
//                }catch(Exception fe){fe.printStackTrace();}
//
//                }if(requestMap.keySet().stream().anyMatch(s->s.contains(stream))){
//                //allRTSPInProgress.remove(stream);
//                streamRecorderController.ring();
//            }
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//            if (requestMap.keySet().stream().anyMatch(s -> s.contains(stream))) {
//                if (new File(outputFile).exists() && new File(outputFile).isFile() && FileUtils.sizeOf(new File(outputFile))>0){
//                    try{
//                        new File(outputFile).renameTo(new File(StringUtils.substringBeforeLast(outputFile, ".flv") + "-ready"
//                            + System.currentTimeMillis() + ".flv"));
//                    }catch(Exception fe){fe.printStackTrace();}
//                }
//                ErrorMeasurement em = new ErrorMeasurement();
//                em.setTime(System.currentTimeMillis());
//                em.setType("videoRecorder");
//                em.setReason("exception while stopping the stream recording");
//                em.setName(stream);
//                Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//                influxDbUtils.influxDB.write(point);
//                streamRecorderController.ring();
//            }
//
//        }
//
//    }
//
//
//
////    public void start(String stream,String output,String streamName){
////        List<String> commands = new java.util.ArrayList<String>();
////        commands.add(ffmpegPath);
////        commands.add("-n");
////        commands.add("-rtsp_transport");
////        commands.add("tcp");
////        commands.add("-v");
////        commands.add("error");
////        commands.add("-rtbufsize");
////        commands.add("300M");
////        commands.add("-i");
////        //commands.add("rtmp://live.chosun.gscdn.com/live/tvchosun1.stream");
////        commands.add(stream);
////        commands.add("-f");
////        commands.add("mp4");
////        commands.add("-vcodec");
////        commands.add("copy");
////        commands.add("-acodec");
////        commands.add("copy");
////        commands.add("-bsf:a");
////        commands.add("aac_adtstoasc");
////        commands.add("-ac");
////        commands.add("1");
////        commands.add(output);
////        //commands.add("/Users/lijunjie/KadanLAB/data/recorde.mp4");
////        ProcessBuilder builder = new ProcessBuilder();
////        builder.command(commands);
////        try {
////            final Process p = builder.start();
////            recordingList.put(streamName,streamName);
////            log.info("recorder starting for ...."  +stream + " ");
////        } catch (Exception e) {
////            log.info("error while recording stream"  +stream + " bringing up....");
////            e.printStackTrace();
////            if(recordingList.containsKey(streamName)){
////                new StreamRecorderController().record(streamName);
////                //recordBuilder(inputRTSP, outputFile,streamName);
////            }
////        }
////    }
//
//    public void concat(String input, String output){
//        FFmpegBuilder builder =
//                new FFmpegBuilder()
//                        .setFormat("concat")
//                        .addInput(input)
//                        .addOutput(output)
//                        .setVideoCodec("copy")
//                        .setAudioCodec("copy")
//                        .setFormat("flv")
//                        .done();
//        FFmpegExecutor executor = null;
//        try {
//            executor = new FFmpegExecutor(ffmpeg,ffprobe);
//        } catch (Exception e) {
//            e.printStackTrace();
//            ErrorMeasurement em = new ErrorMeasurement();
//            em.setTime(System.currentTimeMillis());
//            em.setType("videoRecorder");
//            em.setReason("failed to concat videos");
//            em.setName("videoRecorder");
//            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//            influxDbUtils.influxDB.write(point);
//        }
//        executor.createJob(builder).run();
//    }
//
//
////    public void recordBuilder(String inputRTSP, String outputFile,String streamName) {
////        recordingList.put(streamName,streamName);
////        try{
////            FFmpegBuilder builder1 =
////                    new FFmpegBuilder()
////                            .setInput(inputRTSP)
////                            .overrideOutputFiles(false)
////                            .addOutput(outputFile)
////                            .setDuration(30, TimeUnit.MINUTES)
////                            .setVideoCodec("copy")
////                            .setAudioCodec("copy")
////                            .setAudioChannels(1)
////                            .setAudioBitStreamFilter("aac_adtstoasc")
////                            .setFormat("mp4")
////                            .done();
////            new FFmpegExecutor(ffmpeg,ffprobe).createJob(builder1).run();
////            if(recordingList.containsKey(streamName)){
////                recordingList.remove(streamName);
////                log.info("yet to receive a stop signal, keep recording the stream " + streamName);
////                streamRecorderController.record(streamName);
////                //recordBuilder(inputRTSP, outputFile,streamName);
////            }
////        }catch(Exception e){
////            e.printStackTrace();
////            log.info("error while recording stream"  + streamName + " ");
////            recordingList.remove(streamName);
//////            if(recordingList.containsKey(streamName));{
//////                recordingList.remove(streamName);
//////                streamRecorderController.record(streamName);
//////
//////            }
//////                //recordBuilder(inputRTSP, outputFile,streamName);
////        }
////
////    }
//
//
//    public void tailorVideo(String input, String output,int startOffSet) {
//        log.info("tailoring file with offset " + startOffSet );
//
//        FFmpegBuilder builder =
//                new FFmpegBuilder()
//                        .addInput(input)
//                        .setStartOffset(startOffSet, TimeUnit.SECONDS)
//                        .addOutput(output)
//                        .setVideoCodec("copy")
//                        .setAudioCodec("copy")
//                        .setAudioChannels(1)
//                        .setAudioBitStreamFilter("aac_adtstoasc")
//                        .setFormat("flv")
//                        //.setVideoMovFlags("faststart")
//                        .done();
//        FFmpegExecutor executor = null;
//        try {
//            executor = new FFmpegExecutor(ffmpeg, ffprobe);
//        } catch (Exception e) {
//            e.printStackTrace();
//            ErrorMeasurement em = new ErrorMeasurement();
//            em.setTime(System.currentTimeMillis());
//            em.setType("videoRecorder");
//            em.setReason("failed to tailor video");
//            em.setName("videoRecorder");
//            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//            influxDbUtils.influxDB.write(point);
//
//            log.info("file tailor failed");
//        }
//        executor.createJob(builder).run();
//
//    }
//
//    public void getFirstFrameAsImage(String inputVideo, String image){
//        List<String> commands = new java.util.ArrayList<String>();
//        commands.add(ffmpegPath);
//        commands.add("-i");
//        commands.add(inputVideo);
//        commands.add("-ss");
//        commands.add("1");
//        commands.add("-f");
//        commands.add("image2");
////        commands.add("-vframes");
////        commands.add("1");
//        commands.add(image);
//        ProcessBuilder builder = new ProcessBuilder();
//        builder.command(commands);
//        try {
//            final Process p = builder.start();
//        } catch (Exception e) {
//            e.printStackTrace();
//            ErrorMeasurement em = new ErrorMeasurement();
//            em.setTime(System.currentTimeMillis());
//            em.setType("videoRecorder");
//            em.setReason("failed to get image from video");
//            em.setName("videoRecorder");
//            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//            influxDbUtils.influxDB.write(point);
//        }
//    }
//
//
//    public  int getVideoTime(String video_path) {
//
//        try {
//            FFmpegProbeResult probeResult = ffprobe.probe(video_path);
//            FFmpegFormat format = probeResult.getFormat();
//            double d = format.duration;
//            int duration = Double.valueOf(d).intValue();
//            String dd = String.valueOf(d);
//            if(Integer.valueOf(StringUtils.substringAfter(dd,"."))>0)
//                return (duration+1);
//            else
//                return duration;
//
//        } catch (IOException e) {
//            e.printStackTrace();
//            ErrorMeasurement em = new ErrorMeasurement();
//            em.setTime(System.currentTimeMillis());
//            em.setType("videoRecorder");
//            em.setReason("failed to get video length");
//            em.setName("videoRecorder");
//            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//            influxDbUtils.influxDB.write(point);
//            return 0;
//        }
//
////
////        List<String> commands = new java.util.ArrayList<String>();
////        commands.add(ffmpegPath);
////        commands.add("-i");
////        commands.add(video_path);
////        try {
////            ProcessBuilder builder = new ProcessBuilder();
////            builder.command(commands);
////            final Process p = builder.start();
////
////            //从输入流中读取视频信息
////            BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream()));
////            StringBuffer sb = new StringBuffer();
////            String line = "";
////            while ((line = br.readLine()) != null) {
////                sb.append(line);
////            }
////            br.close();
////
////            //从视频信息中解析时长
////            String regexDuration = "Duration: (.*?), start: (.*?), bitrate: (\\d*) kb\\/s";
////            Pattern pattern = Pattern.compile(regexDuration);
////            Matcher m = pattern.matcher(sb.toString());
////            if (m.find()) {
////                int time = getTimelen(m.group(1));
////                log.info(video_path+ ", length："+time+", start@ ："+m.group(2)+", Bits："+m.group(3)+"kb/s");
////                return time;
////            }
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////
////        return 0;
//    }
//
//    private int getTimelen(String timelen) {
//        int min = 0;
//        String strs[] = timelen.split(":");
//        if (strs[0].compareTo("0") > 0) {
//            min += Integer.valueOf(strs[0]) * 60 * 60;//秒
//        }
//        if (strs[1].compareTo("0") > 0) {
//            min += Integer.valueOf(strs[1]) * 60;
//        }
//        if (strs[2].compareTo("0") > 0) {
//            min += Math.round(Float.valueOf(strs[2]));
//        }
//        return min;
//    }
//
//
//}
