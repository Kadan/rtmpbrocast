//package com.ilabservice.darwinrecorder.hlslive.entity;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.Id;
//
//@Entity
//public class EvtVideo {
//    @Column(nullable = false)
//    public String video;
//    @Column(nullable = true)
//    public String image;
//    @Column(nullable = true)
//    public Long startTime;
//    @Column(nullable = true)
//    public Long endTime;
//    @Column(nullable = true)
//    public Long length;
//
//
//    public String getCameraId() {
//        return cameraId;
//    }
//
//    public void setCameraId(String cameraId) {
//        this.cameraId = cameraId;
//    }
//
//    @Column(nullable = true)
//    public String cameraId;
//    @Id
//    @GeneratedValue
//    private Long id;
//
//
//    public EvtVideo(String video, Long startTime, Long endTime, String image, Long length){
//        this.video = video;
//        this.startTime = startTime;
//        this.endTime = endTime;
//        this.image = image;
//        this.length = length;
//
//    }
////default constructor
//    public EvtVideo(){}
//
//    public String getVideo() {
//        return video;
//    }
//
//    public void setVideo(String video) {
//        this.video = video;
//    }
//
//    public String getImage() {
//        return image;
//    }
//
//    public void setImage(String image) {
//        this.image = image;
//    }
//
//
//    public Long getLength() {
//        return length;
//    }
//
//    public void setLength(Long length) {
//        this.length = length;
//    }
//
//
//
//    public Long getStartTime() {
//        return startTime;
//    }
//
//    public void setStartTime(Long startTime) {
//        this.startTime = startTime;
//    }
//
//    public Long getEndTime() {
//        return endTime;
//    }
//
//    public void setEndTime(Long endTime) {
//        this.endTime = endTime;
//    }
//
//
//}
