package com.ilabservice.darwinrecorder.hlslive.entity;

import io.swagger.models.auth.In;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class PusherTester {


//    public static void main(String[] args) throws InterruptedException {
//
//        double d = 2333.004;
//        d= new BigDecimal(d/60).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
//        System.out.println(d);
//
////        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
////
////        String s = "1563937933000-1563938933999";
////
////        String name = simpleDateFormat.format(new Date(Long.valueOf(StringUtils.substringBefore(s,"-")).longValue())) + "-" +
////                simpleDateFormat.format(new Date(Long.valueOf(StringUtils.substringAfter(s,"-")).longValue()));
////
////        System.out.println(name);
//
////
////
////                String name = simpleDateFormat.format(new Date(Long.valueOf(StringUtils.substringBefore(s,"-")).longValue())) + "-" +
////                simpleDateFormat.format(new Date(Long.valueOf(StringUtils.substringAfter(s,"-")).longValue()));
////
////        System.out.println(Integer.valueOf("00005"));
////
////
////
////        System.out.println( simpleDateFormat.format(new Date(Long.valueOf("1574865564774"))));
////
////        File file = new File("/Users/lijunjie/kadanlab/data/raw-ready00000.mp4");
////        System.out.println(System.currentTimeMillis());
////        file.renameTo(new File("/Users/lijunjie/kadanlab/data/raw-ready000001.mp4"));
////        Thread.sleep(2000);
////        System.out.println(new File("/Users/lijunjie/kadanlab/data/raw-ready000001.mp4").lastModified());
////        try {
////            ppppp();
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
//
////        long inss = System.currentTimeMillis();
//
//
////        Long.valueOf(test);
////
////        String pad = "0000000000000000000";
////         long startTime = 000;
////        if(String.valueOf(startTime).length()<pad.length()){
////            System.out.println(pad);
////            System.out.println(startTime + pad.substring(String.valueOf(startTime).length(),pad.length()));
////            System.out.println("1573570413524628923");
////        }
//
////        Map<String,Integer> testMap = new HashMap<String,Integer>();
////        testMap.put("kadan",22);
////        testMap.put("kammi",24);
////        testMap.put("hello",21);
////        testMap.put("kammi",21);
////
////        System.out.println(testMap.entrySet().stream().max(Map.Entry.comparingByValue()).get().getKey());
////        System.out.println(testMap.entrySet().stream().max(Map.Entry.comparingByValue()).get().getValue());
//
////        Person p = new Person();
////        p.setAge(10);
////        p.setSalary(100);
////        p.setName("hello");
////
////        Person p1 = new Person();
////        p1.setAge(10);
////        p1.setSalary(1001);
////        p1.setName("hello1");
////
////        Person p2 = new Person();
////        p2.setAge(20);
////        p2.setSalary(1001);
////        p2.setName("hello2");
////
////        Person p3 = new Person();
////        p3.setAge(20);
////        p3.setSalary(1001);
////        p3.setName("hello2");
////
////        List<Person> lp = new ArrayList<Person>();
////        lp.add(p1);
////        lp.add(p);
////        lp.add(p2);
////        lp.add(p3);
////
////       Map<Integer, List<Person>> lll = lp.stream().collect(Collectors.groupingBy(Person::getAge));
////
////       lll.keySet().stream().forEach(System.out::println);
//
//
//        //new PusherTester().pushRTMP();
//    }


    static class Person{
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public int getSalary() {
            return salary;
        }

        public void setSalary(int salary) {
            this.salary = salary;
        }

        String name;
        int age;
        int salary;

    }


    private static void ppppp() throws IOException {
        FFmpeg ffmpeg = new FFmpeg("/usr/local/bin/ffmpeg");
        FFprobe ffprobe = new FFprobe("/usr/local/bin/ffprobe");

        FFmpegBuilder builder =
        new FFmpegBuilder()
                .addExtraArgs("-r")
                .addExtraArgs("18")
                .setInput("/Users/lijunjie/kadanlab/data/raw-ready00000.mp4")
                .addOutput("/Users/lijunjie/kadanlab/data/raw-ready00010.mp4")
                .setStartOffset(4, TimeUnit.SECONDS)
                .setDuration(100000000, TimeUnit.SECONDS)
                .setVideoCodec("copy")
                .setAudioCodec("copy")
                .setVideoMovFlags("faststart+frag_keyframe")
//                        .setAudioChannels(1)
//                        .setAudioBitStreamFilter("aac_adtstoasc")
                .setFormat("mp4")
                //.setVideoMovFlags("faststart")
                .addExtraArgs("-reset_timestamps")
                .addExtraArgs("1")
                .addExtraArgs("-map")
                .addExtraArgs("0")
                .done();

        FFmpegExecutor executor = new FFmpegExecutor(ffmpeg, ffprobe);
        executor.createJob(builder).run();
    }


    private static void ppp() throws IOException {
        FFmpeg ffmpeg = new FFmpeg("/usr/local/bin/ffmpeg");
        FFprobe ffprobe = new FFprobe("/usr/local/bin/ffprobe");

        FFmpegBuilder builder =
                new FFmpegBuilder()
                        .addExtraArgs("-r")
                        .addExtraArgs("18")
                        .addExtraArgs("-rtsp_transport")
                        .addExtraArgs("tcp")
                        .setInput("rtsp://40.73.41.176/E1PXA40J")
                        .overrideOutputFiles(true)
                        .addOutput("/Users/lijunjie/kadanlab/data/" + "raw-ready%05d.mp4")
//                            .setDuration(12, TimeUnit.HOURS)
                        .setVideoCodec("copy")
                        .setAudioCodec("copy")
                        .setVideoMovFlags("faststart+frag_keyframe")
//                            .setAudioChannels(1)
//                            .setAudioBitStreamFilter("aac_adtstoasc")
                        .setFormat("segment")
                        .addExtraArgs("-segment_time")
                        .addExtraArgs("3600")
                        .addExtraArgs("-segment_format")
                        .addExtraArgs("mp4")
                        .addExtraArgs("-segment_wrap")
                        .addExtraArgs("24")
                        .addExtraArgs("-segment_start_number")
                        .addExtraArgs("0")
                        .addExtraArgs("-reset_timestamps")
                        .addExtraArgs("1")
                        .addExtraArgs("-map")
                        .addExtraArgs("0")
                        .done();

        FFmpegExecutor executor = new FFmpegExecutor(ffmpeg, ffprobe);
        executor.createJob(builder).run();
    }


        private static void pushRTMP(){

            try {
                FFmpegBuilder builder1 =
                        new FFmpegBuilder()
                                .addExtraArgs("-r")
                                .addExtraArgs("18")
                                .addExtraArgs("-rtsp_transport")
                                .addExtraArgs("tcp")
                                .setInput("rtsp://40.73.41.176/D71028924")
                                .overrideOutputFiles(false)
                                .addOutput("/Users/lijunjie/kadanlab/data/" + "testtt%03d.flv")
//                            .setDuration(12, TimeUnit.HOURS)
                                .setVideoCodec("libx264")
                                .setAudioCodec("copy")
//                            .setAudioChannels(1)
//                            .setAudioBitStreamFilter("aac_adtstoasc")
                                .setFormat("segment")
                                .addExtraArgs("-segment_time")
                                .addExtraArgs("1")
                                .addExtraArgs("-segment_format")
                                .addExtraArgs("flv")
                                .addExtraArgs("-segment_wrap")
                                .addExtraArgs("2")
                                .addExtraArgs("-segment_start_number")
                                .addExtraArgs("0")
                                .done();
                new FFmpegExecutor(new FFmpeg("/usr/local/bin/ffmpeg"), new FFprobe("/usr/local/bin/ffprobe")).createJob(builder1).run();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }