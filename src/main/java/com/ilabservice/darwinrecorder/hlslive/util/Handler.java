package com.ilabservice.darwinrecorder.hlslive.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Handler {
    @Value("${ffmpegpath}")
    private String ffmpeg;

    private static final Logger log = LoggerFactory.getLogger(Handler.class);

    public void Stop(String streamName) {
        List<String> PID = this.getPID(ffmpeg,streamName,"-f segment","-strftime 1 -segment_time 3600 -segment_format mp4 -reset_timestamps 1");
        PID.stream().forEach(s->closeLinuxProcess(s));
    }


    public void stopRTMP(String streamName) {
        List<String> PID = this.getPID(ffmpeg,streamName,"-f flv","-vcodec copy -acodec copy -ac 1 -bsf:a aac_adtstoasc rtmp");
        PID.stream().forEach(s->closeLinuxProcess(s));
    }

    public boolean streamLive(String streamName){
        List<String> PID = this.getPID(ffmpeg,streamName,"-f segment","-strftime 1 -segment_time 3600 -segment_format mp4 -reset_timestamps 1");
        if(PID!= null &&PID.size()>0)
            return true;
        else
            return false;
    }

    public boolean rtmpInProgress(String streamName){
        List<String> PID = this.getPID(ffmpeg,streamName,"-f flv","-vcodec copy -acodec copy -ac 1 -bsf:a aac_adtstoasc rtmp");
        if(PID!= null &&PID.size()>0)
            return true;
        else
            return false;
    }

    /**
     * 获取Linux进程的PID
     * @param command
     * @return
     */
    public List<String> getPID(String command, String key, String key1, String key2){
        BufferedReader reader =null;
        List<String> PIDs = new ArrayList<String>();
        try{
            //显示所有进程
            Process process = Runtime.getRuntime().exec("ps -ef");
            reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = null;
            while((line = reader.readLine())!=null){
                if(line.contains(command) && line.contains(key) && line.contains(key1)&& line.contains(key2)){
//                   log.info("found processs "+line);
                    String[] strs = line.split("\\s+");
//                    log.info("strs[0] "+strs[0]);
//                    log.info("strs[1] "+strs[1]);
//                    log.info("strs[2] "+strs[2]);
                    PIDs.add(strs[1]);
//                    PIDs.add(strs[2]);
                }
            }
            return PIDs;
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            if(reader!=null){
                try {
                    reader.close();
                } catch (IOException e) {

                }
            }
        }
        return PIDs;
    }

    /**
     * 关闭Linux进程
     * @param Pid 进程的PID
     */
    public void closeLinuxProcess(String Pid){
        Process process = null;
        BufferedReader reader =null;
        try{
            //kill the process
            process = Runtime.getRuntime().exec("kill -15 "+Pid);
            reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = null;
            while((line = reader.readLine())!=null){
                log.info("kill PID return info -----> "+line);
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            if(process!=null){
                process.destroy();
            }

            if(reader!=null){
                try {
                    reader.close();
                } catch (IOException e) {

                }
            }
        }
    }

    public int getDuration(long startTime, long endTime){
        long diff;
        if (startTime < endTime) {
            diff = endTime - startTime;
        } else {
            diff = 0;
        }

        return Long.valueOf(diff%1000==0?diff/1000:diff/1000+1).intValue();
    }
}